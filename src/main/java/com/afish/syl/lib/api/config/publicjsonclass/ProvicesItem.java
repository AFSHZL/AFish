package com.afish.syl.lib.api.config.publicjsonclass;

import net.tsz.afinal.annotation.sqlite.Id;
import net.tsz.afinal.annotation.sqlite.Table;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2014-12-24.
 */

@Table(name = "provicesTable")
public class ProvicesItem {
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Id(column = "id")

    private Integer id;
    private String region_id;
    private String region_sn;
    private String region_name;
    private String region_outer_name;
    private List<CityItem> city = new ArrayList<CityItem>();

    public String getRegion_sn() {
        return region_sn;
    }

    public void setRegion_sn(String region_sn) {
        this.region_sn = region_sn;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    public String getRegion_outer_name() {
        return region_outer_name;
    }

    public void setRegion_outer_name(String region_outer_name) {
        this.region_outer_name = region_outer_name;
    }

    public List<CityItem> getCity() {
        return city;
    }

    public void setCity(List<CityItem> city) {
        this.city = city;
    }

    public String getRegion_id() {
        return region_id;
    }

    public void setRegion_id(String region_id) {
        this.region_id = region_id;
    }
}
