package com.afish.syl.lib.api.config.publicjsonclass;

/**
 * Created by Administrator on 2014-12-30.
 * 报表显示字段类
 */
public class FieldParam {
    //字段代码
    private String field;
    //字段名称
    private String fieldName;
    //大小屏幕占比
    private Integer size;
    //是否显示合计
    private boolean isSum;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public boolean isSum() {
        return isSum;
    }

    public void setSum(boolean isSum) {
        this.isSum = isSum;
    }
}
