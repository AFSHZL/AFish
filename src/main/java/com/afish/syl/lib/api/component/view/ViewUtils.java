package com.afish.syl.lib.api.component.view;

import android.view.View;
import android.view.ViewGroup.LayoutParams;

/**
 * @author 苏逸龙     317616660@qq.com
 * @ClassName: ImageViewUtils
 * @Description: TODO(设置View的各种参数)
 * @date 2014-3-10 下午7:04:32
 */
public class ViewUtils {

    /**
     * MethodName:setViewHeight
     * Method description
     *
     *
     * @param v
     * @param iHeight
     */
    public static void setViewHeight(View v, Integer iHeight) {
        LayoutParams viewParams;

        viewParams        = v.getLayoutParams();
        viewParams.height = iHeight;
        v.setLayoutParams(viewParams);
    }

    /**
     * MethodName:setViewMatchHeight
     * Method description
     *
     *
     * @param v
     */
    public static void setViewMatchHeight(View v) {
        LayoutParams viewParams;

        viewParams        = v.getLayoutParams();
        viewParams.height = LayoutParams.MATCH_PARENT;
        v.setLayoutParams(viewParams);
    }

    /**
     * MethodName:setViewMatchWidth
     * Method description
     *
     *
     * @param v
     */
    public static void setViewMatchWidth(View v) {
        LayoutParams viewParams;

        viewParams       = v.getLayoutParams();
        viewParams.width = LayoutParams.MATCH_PARENT;
        v.setLayoutParams(viewParams);
    }

    /**
     *
     * @param v
     * @param iWidth
     * @Title: setImageWidth
     * @Description: TODO(设置View的宽)
     */
    public static void setViewWidth(View v, Integer iWidth) {
        LayoutParams viewParams;

        viewParams       = v.getLayoutParams();
        viewParams.width = iWidth;
        v.setLayoutParams(viewParams);
    }

    /**
     * MethodName:setViewWrapHeight
     * Method description
     * 设置一个View的高度
     *
     * @param v
     */
    public static void setViewWrapHeight(View v) {
        LayoutParams viewParams;

        viewParams        = v.getLayoutParams();
        viewParams.height = LayoutParams.WRAP_CONTENT;
        v.setLayoutParams(viewParams);
    }

    /**
     * MethodName:setViewWrapWidth
     * Method description
     * 设置一个View的宽度
     *
     * @param v
     */
    public static void setViewWrapWidth(View v) {
        LayoutParams viewParams;

        viewParams       = v.getLayoutParams();
        viewParams.width = LayoutParams.WRAP_CONTENT;
        v.setLayoutParams(viewParams);
    }

//  public static void ViewLoading(Activity activity) {
//      LoadingView loadingView = (LoadingView)activity.findViewById(R.id.loadView);
//      if (OUtils.IsNotNull(loadingView)) {
//          loadingView.setVisibility(View.VISIBLE);
//      }
//  }
//
//  public static void ViewLoadCancle(Activity activity) {
//      LoadingView loadingView = (LoadingView)activity.findViewById(R.id.loadView);
//      if (OUtils.IsNotNull(loadingView)) {
//          loadingView.setVisibility(View.GONE);
//      }
//  }
}


//~ Formatted by syl
