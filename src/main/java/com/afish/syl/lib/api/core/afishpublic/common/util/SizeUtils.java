package com.afish.syl.lib.api.core.afishpublic.common.util;

/**
 * SizeUtils
 * 大小单位单元
 * 
 * @author  2013-5-15
 */
public class SizeUtils {

    /** gb to byte **/
    public static final long GB_2_BYTE = 1073741824;
    /** mb to byte **/
    public static final long MB_2_BYTE = 1048576;
    /** kb to byte **/
    public static final long KB_2_BYTE = 1024;

    private SizeUtils() {
        throw new AssertionError();
    }
}
