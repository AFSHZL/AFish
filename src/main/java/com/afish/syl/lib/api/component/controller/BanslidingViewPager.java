package com.afish.syl.lib.api.component.controller;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * @author 苏逸龙     317616660@qq.com
 * @ClassName: BanslidingViewPager
 * @Description: TODO(重写的禁用滑动的ViewPager控件类)
 * 让ViewPager与ScrollView共存
 * @date 2014-3-18 下午5:43:31
 */
public class BanslidingViewPager extends ViewPager {


    private boolean enabled;


    public BanslidingViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.enabled = false;
    }

    //触摸没有反应就可以了
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (this.enabled) {
            return super.onTouchEvent(event);
        }

        return false;
    }


    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (this.enabled) {
            return super.onInterceptTouchEvent(event);
        }

        return false;
    }

    public void setPagingEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}


