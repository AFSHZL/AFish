package com.afish.syl.lib.api.afishlib.baidumaps;

import android.content.Context;


public class BaiduLocationServiceImpl implements ILocationService {

//	public static final String strKey = "C2D64E1326E10EAC4641DAF74FD5A9520A9A074C";

//	private LocationClient mLocationClient;


//	private MKSearch mSearch;

	private Context context;

	// private List<NameValuePair> locInfo;

	private LocationInfo locationInfo;

	private ILocationCallback callback;

	public BaiduLocationServiceImpl(Context context, ILocationCallback callback) {
		this.context = context;
		this.callback = callback;
		// 初始化定位引擎
		this.initLocationClient();
		this.initEngineManager();
		this.initMKSearch();
	}

	// 设置相关参数
	private void initLocationClient() {
//		mLocationClient = new LocationClient(context);
//		mLocationClient.registerLocationListener(new MyLocationListenner());
//		LocationClientOption option = new LocationClientOption();
//		option.setOpenGps(true); // 打开gps
//		option.setCoorType("bd09ll"); // 设置坐标类型
//		option.setAddrType("all");
//		// option.setScanSpan(3000);
//		mLocationClient.setLocOption(option);
	}

	@Override
	public void locate() {
		// 获取经纬度
//		if (!mLocationClient.isStarted()) {
//			mLocationClient.start();
//		}
//		mLocationClient.requestLocation();
	}

	@Override
	public void locateStop() {
		// 关闭定位
//		if (mLocationClient.isStarted()) {
//			mLocationClient.stop();
//		}
	}

	/**
	 * 监听函数，又新位置的时候，格式化成字符串，输出到屏幕中
	 */
//	public class MyLocationListenner implements BDLocationListener {
//		@Override
//		public void onReceiveLocation(BDLocation location) {
//			if (location == null) {
//				Log.i("定位", "定位信息为空");
//				return;
//			}
//
//
//			locationInfo = new LocationInfo();
//			// 纬度
//			locationInfo.setLatitude(location.getLatitude());
//			// 经度
//			locationInfo.setLongitude(location.getLongitude());
//			// 高度
//			locationInfo.setAltitude(location.getAltitude());
//			// 速度
//			locationInfo.setSpeed(Double.valueOf(location.getSpeed()));
//			// 半径
//			locationInfo.setRadius(Double.valueOf(location.getRadius()));
//			// 坐标系
//			locationInfo.setCoorType(location.getCoorType());
//
//			String fdLocType = "";
//			if (location.getLocType() == BDLocation.TypeGpsLocation) {
//				fdLocType = "GPS";
//				locationInfo.setLocType(fdLocType);
//				// GPS定位，解析地址信息
//				Double latitude = location.getLatitude() * 1e6;
//				Double Longitude = location.getLongitude() * 1e6;
//			} else if (location.getLocType() == BDLocation.TypeNetWorkLocation) {
//				fdLocType = "NETWORK";
//				locationInfo.setLocType(fdLocType);
//				// 网络定位，直接取得地址信息
////				String fdAddrStr = LocUtil.getAddr(location); // location.getAddrStr()
//				// 地址
////				locationInfo.setAddrStr(fdAddrStr);
//				// 省
//				locationInfo.setProvince(location.getProvince());
//				// 市
//				locationInfo.setCity(location.getCity());
//				// 区号
//				locationInfo.setCityCode(location.getCityCode());
//				// 区
//				locationInfo.setDistrict(location.getDistrict());
//				// 街道
//				locationInfo.setStreet(location.getStreet());
//				// 街道号码
//				locationInfo.setStreetNum(location.getStreetNumber());
//				callback.callback(locationInfo);
//			}
//			Log.i("定位类型：", fdLocType);
//			// mLocationClient.stop();
//		}
//
//		public void onReceivePoi(BDLocation poiLocation) {
//		}
//	}

	public void initEngineManager() {
	}

	private void initMKSearch() {
	}


}
