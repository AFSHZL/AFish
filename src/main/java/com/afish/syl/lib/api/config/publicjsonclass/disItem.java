package com.afish.syl.lib.api.config.publicjsonclass;

/**
 * Created by Administrator on 2015-03-08.
 */
public class disItem {
    private String id;
    private String p_id;
    private String region_name;
    private String region_type;
    private String region_outer_name;
    private String region_nick;
    private String pinyin;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getP_id() {
        return p_id;
    }

    public void setP_id(String p_id) {
        this.p_id = p_id;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    public String getRegion_type() {
        return region_type;
    }

    public void setRegion_type(String region_type) {
        this.region_type = region_type;
    }

    public String getRegion_outer_name() {
        return region_outer_name;
    }

    public void setRegion_outer_name(String region_outer_name) {
        this.region_outer_name = region_outer_name;
    }

    public String getRegion_nick() {
        return region_nick;
    }

    public void setRegion_nick(String region_nick) {
        this.region_nick = region_nick;
    }

    public String getPinyin() {
        return pinyin;
    }

    public void setPinyin(String pinyin) {
        this.pinyin = pinyin;
    }
}
