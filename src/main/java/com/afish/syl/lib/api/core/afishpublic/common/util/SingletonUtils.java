package com.afish.syl.lib.api.core.afishpublic.common.util;

/**
 * Singleton helper class for lazily initialization.
 * 
 * @author
 * 
 * @param <T>
 */
public abstract class SingletonUtils<T> {

    private T instance;

    protected abstract T newInstance();

    public final T getInstance() {
        if (instance == null) {
            synchronized (SingletonUtils.class) {
                if (instance == null) {
                    instance = newInstance();
                }
            }
        }
        return instance;
    }
}
