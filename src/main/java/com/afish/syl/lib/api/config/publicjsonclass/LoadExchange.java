package com.afish.syl.lib.api.config.publicjsonclass;

/**
 * Created by Administrator on 2015-08-15.
 */
public class LoadExchange {
    String endtime;
    String halfpic;
    String picpath;
    String picpath_width;
    String picpath_height;
    String exchangeid;
    String begintime;
    String begintimes;
    String endtimes;
    boolean hdq;

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public String getHalfpic() {
        return halfpic;
    }

    public void setHalfpic(String halfpic) {
        this.halfpic = halfpic;
    }

    public String getPicpath() {
        return picpath;
    }

    public void setPicpath(String picpath) {
        this.picpath = picpath;
    }

    public String getPicpath_width() {
        return picpath_width;
    }

    public void setPicpath_width(String picpath_width) {
        this.picpath_width = picpath_width;
    }

    public String getPicpath_height() {
        return picpath_height;
    }

    public void setPicpath_height(String picpath_height) {
        this.picpath_height = picpath_height;
    }

    public String getExchangeid() {
        return exchangeid;
    }

    public void setExchangeid(String exchangeid) {
        this.exchangeid = exchangeid;
    }

    public String getBegintime() {
        return begintime;
    }

    public void setBegintime(String begintime) {
        this.begintime = begintime;
    }

    public String getBegintimes() {
        return begintimes;
    }

    public void setBegintimes(String begintimes) {
        this.begintimes = begintimes;
    }

    public String getEndtimes() {
        return endtimes;
    }

    public void setEndtimes(String endtimes) {
        this.endtimes = endtimes;
    }

    public boolean getHdq() {
        return hdq;
    }

    public void setHdq(boolean hdq) {
        this.hdq = hdq;
    }
}
