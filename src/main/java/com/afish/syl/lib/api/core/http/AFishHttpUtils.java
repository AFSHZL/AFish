package com.afish.syl.lib.api.core.http;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * ClassName: AFishHttpUtils
 * Class description
 * 一些简单的http操作
 *
 * @version        1.0, 16/01/27
 * @author         syl    
 */
public class AFishHttpUtils {

    /** Field 上下文 */
    private Context FContext;

    /**
     * Constructs ...
     *
     *
     * @param context
     */
    public AFishHttpUtils(Context context) {
        FContext = context;
    }

    /**
     * MethodName:PostUrlApiJsonStr
     * Method description
     * 最基础的POST
     *
     * @param urlString
     * @param postString
     *
     * @return
     */
    public String PostUrlApiJsonStr(String urlString, String postString) {
        String jsonString = "";
        URL    apiUrl     = null;

        if (isNetworkConnected()) {
            try {
                apiUrl = new URL(urlString);

                HttpURLConnection jsonConn = (HttpURLConnection) apiUrl.openConnection();

                jsonConn.setReadTimeout(10000);
                jsonConn.setConnectTimeout(5000);
                jsonConn.setRequestMethod("POST");
                jsonConn.connect();

                DataOutputStream outputStream = new DataOutputStream(jsonConn.getOutputStream());

                outputStream.writeBytes(postString);
                outputStream.flush();
                outputStream.close();

                InputStream inputStream = jsonConn.getInputStream();
                byte[]      data;

                data       = StreamTool.ReadInputSream(inputStream);
                jsonString = new String(data);
            } catch (MalformedURLException e) {

                // TODO Auto-generated catch block
                return jsonString;
            } catch (IOException e) {

                // TODO Auto-generated catch block
                return jsonString;
            } catch (Exception e) {

                // TODO Auto-generated catch block
                return jsonString;
            }
        } else {
            jsonString = "";
        }

        return jsonString;
    }

    /**
     * MethodName:getConnectedType
     * Method description
     * 获取连接类型
     *
     * @return
     */
    public int getConnectedType() {
        if (FContext != null) {
            ConnectivityManager mConnectivityManager =
                (ConnectivityManager) FContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();

            if ((mNetworkInfo != null) && mNetworkInfo.isAvailable()) {
                return mNetworkInfo.getType();
            }
        }

        return -1;
    }

    /**
     * MethodName:get
     * Method description
     * 最基础的Get方法
     *
     * @param urlString
     *
     * @return
     */
    public String get(String urlString) {
        String jsonString = "";
        URL    apiUrl     = null;

        if (isNetworkConnected()) {
            try {
                apiUrl = new URL(urlString);

                HttpURLConnection jsonConn = (HttpURLConnection) apiUrl.openConnection();

                jsonConn.setReadTimeout(30000);
                jsonConn.setConnectTimeout(8000);
                jsonConn.setRequestMethod("GET");

                InputStream inputStream = jsonConn.getInputStream();
                byte[]      data;

                data       = StreamTool.ReadInputSream(inputStream);
                jsonString = new String(data);
            } catch (MalformedURLException e) {

                // TODO Auto-generated catch block
                return jsonString;
            } catch (IOException e) {

                // TODO Auto-generated catch block
                return jsonString;
            } catch (Exception e) {

                // TODO Auto-generated catch block
                return jsonString;
            }
        } else {
            jsonString = "";
        }

        return jsonString.replace("\ufeff", "");
    }

    // 判断MOBILE网络是否可用

    /**
     * MethodName:isMobileConnected
     * Method description
     * 判断手机网络是否可用
     *
     * @return
     */
    public boolean isMobileConnected() {
        if (FContext != null) {
            ConnectivityManager mConnectivityManager =
                (ConnectivityManager) FContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mMobileNetworkInfo = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if (mMobileNetworkInfo != null) {
                return mMobileNetworkInfo.isAvailable();
            }
        }

        return false;
    }

    // 判断是否有网络连接

    /**
     * MethodName:isNetworkConnected
     * Method description
     * 判断是否有网络连接
     *
     * @return
     */
    public boolean isNetworkConnected() {
        if (FContext != null) {
            ConnectivityManager mConnectivityManager =
                (ConnectivityManager) FContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();

            if (mNetworkInfo != null) {
                return mNetworkInfo.isAvailable();
            }
        }

        return false;
    }

    /**
     * MethodName:getUrlJsonString
     * Method description
     * 最基础的获取Get
     *
     * @param urlString
     *
     * @return
     */
    public String getUrlJsonString(String urlString) {
        String jsonString = "";
        URL    apiUrl     = null;

        if (isNetworkConnected()) {
            try {
                apiUrl = new URL(urlString);

                HttpURLConnection jsonConn = (HttpURLConnection) apiUrl.openConnection();

                jsonConn.setReadTimeout(30000);
                jsonConn.setConnectTimeout(8000);
                jsonConn.setRequestMethod("GET");

                InputStream inputStream = jsonConn.getInputStream();
                byte[]      data;

                data       = StreamTool.ReadInputSream(inputStream);
                jsonString = new String(data);
            } catch (MalformedURLException e) {

                // TODO Auto-generated catch block
                return jsonString;
            } catch (IOException e) {

                // TODO Auto-generated catch block
                return jsonString;
            } catch (Exception e) {

                // TODO Auto-generated catch block
                return jsonString;
            }
        } else {
            jsonString = "";
        }

        return jsonString.replace("\ufeff", "");
    }

    // 判断WIFI网络是否可用

    /**
     * MethodName:isWifiConnected
     * Method description
     * 判断现在手否是wifi连接
     *
     * @return
     */
    public boolean isWifiConnected() {
        if (FContext != null) {
            ConnectivityManager mConnectivityManager =
                (ConnectivityManager) FContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWiFiNetworkInfo = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            if (mWiFiNetworkInfo != null) {
                return mWiFiNetworkInfo.isAvailable();
            }
        }

        return false;
    }
}


//~ Formatted by syl