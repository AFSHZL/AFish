package com.afish.syl.lib.api.core.otto;

/**
 * 需要定位信息时发送该消息
 * 作者：苏逸龙 on 2015-09-01 20:39
 * QQ：317616660
 */
public class NeedLocEvent {
    String msg;
    String json;

    public NeedLocEvent(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }
}
