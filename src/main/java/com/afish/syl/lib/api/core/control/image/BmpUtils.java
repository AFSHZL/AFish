package com.afish.syl.lib.api.core.control.image;

import android.content.Context;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * 图片处理类
 * 作者：苏逸龙 on 2015-09-12 11:17
 *
 * QQ：317616660
 */
public class BmpUtils {

    /**
     * MethodName:getResIdDrawableHeight
     * Method description
     * 获取一个资源ID图片文件的高度
     *
     * @param context
     * @param iResId
     *
     * @return
     */
    public static int getResIdDrawableHeight(Context context, int iResId) {
        int iRes = 0;

        try {
            Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), iResId);

            iRes = bitmap.getHeight();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return iRes;
    }

    /**
     * MethodName:getResIdDrawableWidth
     * Method description
     * 获取一个资源ID图片的宽度
     *
     * @param context
     * @param iResId
     *
     * @return
     */
    public static int getResIdDrawableWidth(Context context, int iResId) {
        int iRes = 0;

        try {
            Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), iResId);

            iRes = bitmap.getWidth();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return iRes;
    }
}


//~ Formatted by syl