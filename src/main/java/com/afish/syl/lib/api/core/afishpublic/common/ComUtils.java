package com.afish.syl.lib.api.core.afishpublic.common;

import android.app.Activity;

import android.content.Context;

/**
 * 公共工具类
 * 作者：苏逸龙 on 2015-08-28 14:56
 * QQ：317616660
 */
public class ComUtils {

    /**
     * 获取一个Activity的类名
     *
     * @param activity
     * @return
     */
    public static String getActClass(Activity activity) {
        return activity.getClass().getName();
    }

//  /**
//   * 自动识别Otto发过来的广播
//   * @param activityMsgEvent
//   * 专用消息
//   * @param activity
//   * 一个Activity 防止字符串
//   * @return
//   */
//  public static boolean MsgEventEq(ActivityMsgEvent activityMsgEvent,Activity activity) {
//      boolean res = false;
//      if (OUtils.IsNotNull(activityMsgEvent)) {
//          String sActClass = StrUtils.IsNull(getActClass(activity));
//          String sMsg = StrUtils.IsNull(activityMsgEvent.getMsg());
//          if (StrUtils.IsNotEmpty(sActClass) && StrUtils.IsNotEmpty(sMsg)) {
//              if (sActClass.equals(sMsg)) {
//                  res = true;
//              }
//          }
//      }
//      return res;
//  }

    /**
     * MethodName:getAppInfo
     * Method description
     * 获取APP信息
     *
     * @param context
     *
     * @return
     */
    public static String getAppInfo(Context context) {
        try {
            String pkName      = context.getPackageName();
            String versionName = context.getPackageManager().getPackageInfo(pkName, 0).versionName;
            int    versionCode = context.getPackageManager().getPackageInfo(pkName, 0).versionCode;

            return pkName + "   " + versionName + "  " + versionCode;
        } catch (Exception e) {}

        return null;
    }

    /**
     * MethodName:getVersionName
     * Method description
     * 获取版本名称用来显示
     *
     * @param context
     *
     * @return
     */
    public static String getVersionName(Context context) {
        try {
            String pkName      = context.getPackageName();
            String versionName = context.getPackageManager().getPackageInfo(pkName, 0).versionName;
            int    versionCode = context.getPackageManager().getPackageInfo(pkName, 0).versionCode;

            return versionName;
        } catch (Exception e) {}

        return "";
    }
}


//~ Formatted by syl