package com.afish.syl.lib.api.config.publicjsonclass;

/**
 * Created by Administrator on 2015-08-13.
 */
public class LoadCategoryItem {
    String name;
    String flag;
    String categoryid;
    String halfPic;
    String picpath;
    int halfPic_width;
    int halfPic_height;
    int picpath_width;
    int picpath_height;
    int integral;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(String categoryid) {
        this.categoryid = categoryid;
    }

    public String getHalfPic() {
        return halfPic;
    }

    public void setHalfPic(String halfPic) {
        this.halfPic = halfPic;
    }

    public String getPicpath() {
        return picpath;
    }

    public void setPicpath(String picpath) {
        this.picpath = picpath;
    }

    public int getIntegral() {
        return integral;
    }

    public void setIntegral(int integral) {
        this.integral = integral;
    }


}
