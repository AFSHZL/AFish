package com.afish.syl.lib.api.component.adpter.comadpter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

/**
 * ClassName: CommonAdapter
 * Class description
 * 万能适配器
 *
 * @param <T>
 *
 * @version        1.0, 16/01/27
 * @author         syl    
 */
public abstract class CommonAdapter<T> extends BaseAdapter {

    /** Field 布局加载器 */
    protected LayoutInflater mInflater;

    /** Field 上下文 */
    protected Context mContext;

    /** Field 数据List */
    protected List<T> mDatas;

    /** Field Item布局 */
    protected final int mItemLayoutId;

    /**
     * Constructs ...
     *
     *
     * @param context
     * @param mDatas
     * @param itemLayoutId
     */
    public CommonAdapter(Context context, List<T> mDatas, int itemLayoutId) {
        this.mContext      = context;
        this.mInflater     = LayoutInflater.from(mContext);
        this.mDatas        = mDatas;
        this.mItemLayoutId = itemLayoutId;
    }

    /**
     * MethodName:convert
     * Method description
     * 抽象函数,在子类实现设置Item数据内容
     *
     * @param helper
     * @param item
     */
    public abstract void convert(ViewHolder helper, T item);

    @Override
    public int getCount() {
        return mDatas.size();
    }

    @Override
    public T getItem(int position) {
        return mDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder = getViewHolder(position, convertView, parent);

        convert(viewHolder, getItem(position));

        return viewHolder.getConvertView();
    }

    /**
     * MethodName:getViewHolder
     * Method description
     * 获取ViewHolder
     *
     * @param position
     * @param convertView
     * @param parent
     *
     * @return
     */
    private ViewHolder getViewHolder(int position, View convertView, ViewGroup parent) {
        return ViewHolder.get(mContext, convertView, parent, mItemLayoutId, position);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
