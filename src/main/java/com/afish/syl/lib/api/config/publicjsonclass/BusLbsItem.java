package com.afish.syl.lib.api.config.publicjsonclass;

/**
 * Created by Administrator on 215-3-1.
 */
public class BusLbsItem {
    private String id;
    private String business_sn;
    private String business_name;
    private String reg_time;
    private String qq;
    private String tel;
    private String moblie_phone;
    private String reg_type;
    private String is_special;
    private String bs_rank;
    private String is_warn;
    private String address;
    private String fw_fjsx1;
    private String sample_note;
    private String work_starttime;
    private String note;
    private String work_endtime;
    private String user_name;
    private String province4;
    private String city;
    private String dis;
    private String pinglun;
    private String image_url;
    private double lbsjd;
    private double lbswd;
    private String sx_name;
    private String sx_name2;
    private String max_distance;
    private String distance;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBusiness_sn() {
        return business_sn;
    }

    public void setBusiness_sn(String business_sn) {
        this.business_sn = business_sn;
    }

    public String getBusiness_name() {
        return business_name;
    }

    public void setBusiness_name(String business_name) {
        this.business_name = business_name;
    }

    public String getReg_time() {
        return reg_time;
    }

    public void setReg_time(String reg_time) {
        this.reg_time = reg_time;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getMoblie_phone() {
        return moblie_phone;
    }

    public void setMoblie_phone(String moblie_phone) {
        this.moblie_phone = moblie_phone;
    }

    public String getReg_type() {
        return reg_type;
    }

    public void setReg_type(String reg_type) {
        this.reg_type = reg_type;
    }

    public String getIs_special() {
        return is_special;
    }

    public void setIs_special(String is_special) {
        this.is_special = is_special;
    }

    public String getBs_rank() {
        return bs_rank;
    }

    public void setBs_rank(String bs_rank) {
        this.bs_rank = bs_rank;
    }

    public String getIs_warn() {
        return is_warn;
    }

    public void setIs_warn(String is_warn) {
        this.is_warn = is_warn;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFw_fjsx1() {
        return fw_fjsx1;
    }

    public void setFw_fjsx1(String fw_fjsx1) {
        this.fw_fjsx1 = fw_fjsx1;
    }

    public String getSample_note() {
        return sample_note;
    }

    public void setSample_note(String sample_note) {
        this.sample_note = sample_note;
    }

    public String getWork_starttime() {
        return work_starttime;
    }

    public void setWork_starttime(String work_starttime) {
        this.work_starttime = work_starttime;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getWork_endtime() {
        return work_endtime;
    }

    public void setWork_endtime(String work_endtime) {
        this.work_endtime = work_endtime;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getProvince4() {
        return province4;
    }

    public void setProvince4(String province4) {
        this.province4 = province4;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDis() {
        return dis;
    }

    public void setDis(String dis) {
        this.dis = dis;
    }

    public String getPinglun() {
        return pinglun;
    }

    public void setPinglun(String pinglun) {
        this.pinglun = pinglun;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public double getLbsjd() {
        return lbsjd;
    }

    public void setLbsjd(double lbsjd) {
        this.lbsjd = lbsjd;
    }

    public double getLbswd() {
        return lbswd;
    }

    public void setLbswd(double lbswd) {
        this.lbswd = lbswd;
    }

    public String getSx_name() {
        return sx_name;
    }

    public void setSx_name(String sx_name) {
        this.sx_name = sx_name;
    }

    public String getSx_name2() {
        return sx_name2;
    }

    public void setSx_name2(String sx_name2) {
        this.sx_name2 = sx_name2;
    }

    public String getMax_distance() {
        return max_distance;
    }

    public void setMax_distance(String max_distance) {
        this.max_distance = max_distance;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }
}
