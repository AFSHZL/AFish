package com.afish.syl.lib.api.config.publicjsonclass;

/**
 * Created by Administrator on 2015-08-06.
 */
public class LoginUserInfo {
    String EXT_USER_ID;
    String PASS_WORD;
    String USER_NAME;
    String PHONE_NUM;
    String EMAIL;
    String WECHAT_ID;
    String ALIPAY_ID;
    String STATUS;
    String REG_TIME;

    public String getEXT_USER_ID() {
        return EXT_USER_ID;
    }

    public void setEXT_USER_ID(String EXT_USER_ID) {
        this.EXT_USER_ID = EXT_USER_ID;
    }

    public String getPASS_WORD() {
        return PASS_WORD;
    }

    public void setPASS_WORD(String PASS_WORD) {
        this.PASS_WORD = PASS_WORD;
    }

    public String getUSER_NAME() {
        return USER_NAME;
    }

    public void setUSER_NAME(String USER_NAME) {
        this.USER_NAME = USER_NAME;
    }

    public String getPHONE_NUM() {
        return PHONE_NUM;
    }

    public void setPHONE_NUM(String PHONE_NUM) {
        this.PHONE_NUM = PHONE_NUM;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public String getWECHAT_ID() {
        return WECHAT_ID;
    }

    public void setWECHAT_ID(String WECHAT_ID) {
        this.WECHAT_ID = WECHAT_ID;
    }

    public String getALIPAY_ID() {
        return ALIPAY_ID;
    }

    public void setALIPAY_ID(String ALIPAY_ID) {
        this.ALIPAY_ID = ALIPAY_ID;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getREG_TIME() {
        return REG_TIME;
    }

    public void setREG_TIME(String REG_TIME) {
        this.REG_TIME = REG_TIME;
    }
}
