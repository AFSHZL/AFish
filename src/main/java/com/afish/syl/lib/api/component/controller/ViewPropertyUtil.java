package com.afish.syl.lib.api.component.controller;

import android.support.v4.view.ViewPager;

import android.view.View;
import android.view.ViewGroup.LayoutParams;

/**
 * ClassName: ViewPropertyUtil
 * Class description
 * View的操作类
 *
 * @version        1.0, 16/01/27
 * @author         syl    
 */
public class ViewPropertyUtil {

    /**
     * MethodName:setViewAlpha
     * Method description
     * 设置View透明度
     *
     * @param view
     * @param alpha
     */
    public static void setViewAlpha(View view, int alpha) {
        view.getBackground().setAlpha(alpha);
    }

    /**
     * MethodName:setViewPageHeight
     * Method description
     * 设置视图高度
     *
     * @param vp
     * @param heightInteger
     */
    public static void setViewPageHeight(ViewPager vp, Integer heightInteger) {
        LayoutParams mLayoutParams;

        mLayoutParams        = vp.getLayoutParams();
        mLayoutParams.height = heightInteger;
        vp.setLayoutParams(mLayoutParams);
    }

    /**
     * MethodName:setViewPageWidth
     * Method description
     * 设置视图宽度
     *
     * @param vp
     * @param widthInteger
     */
    public static void setViewPageWidth(ViewPager vp, Integer widthInteger) {
        LayoutParams mLayoutParams;

        mLayoutParams       = vp.getLayoutParams();
        mLayoutParams.width = widthInteger;
        vp.setLayoutParams(mLayoutParams);
    }
}


//~ Formatted by syl
