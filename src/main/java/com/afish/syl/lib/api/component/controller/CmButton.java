package com.afish.syl.lib.api.component.controller;

/**
 * 按钮只要默认放一张底图,继承该类的按钮被按下的时候底图会自动变暗
 * CmButton  该类有BUG已更新成JwButton
 * Created by 苏逸龙 on 2015-08-11.
 */

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;

public class CmButton extends ImageButton implements OnTouchListener, OnFocusChangeListener {

    public CmButton(Context context) {
        super(context);
        this.setOnTouchListener(this);
        this.setOnFocusChangeListener(this);
    }

    public CmButton(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.imageButtonStyle);
        this.setOnTouchListener(this);
        this.setOnFocusChangeListener(this);
    }

    public CmButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFocusable(true);
        this.setOnTouchListener(this);
        this.setOnFocusChangeListener(this);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        // ��ɫЧ��
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        if (hasFocus) {
            ((ImageButton) v).getDrawable().setColorFilter(new ColorMatrixColorFilter(cm));
        } else {
            ((ImageButton) v).getDrawable().clearColorFilter();
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        // ��ɫЧ��
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            ((ImageButton) v).getDrawable().setColorFilter(new ColorMatrixColorFilter(cm));
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            ((ImageButton) v).getDrawable().clearColorFilter();
        }
        return false;
    }
}
