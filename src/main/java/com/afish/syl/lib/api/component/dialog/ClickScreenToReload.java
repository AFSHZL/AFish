package com.afish.syl.lib.api.component.dialog;

/**
 * 作者：苏逸龙 on 2015-09-08 09:10
 * QQ：317616660
 */


import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.afish.syl.lib.R;


public class ClickScreenToReload extends View {
    private View view;
    private Context context;

    public ClickScreenToReload(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        // TODO Auto-generated constructor stub
        init();
    }

    public ClickScreenToReload(Context context) {
        super(context);
        this.context = context;
        // TODO Auto-generated constructor stub
        init();
    }

    private RelativeLayout loadingLayout, reloadlayout;
    public Reload reload;

    private void init() {
        reload = new Reload() {
            @Override
            public void clicktoreload() {
                // TODO Auto-generated method stub

            }
        };
//        view = LayoutInflater.from(context).inflate(R.layout.clickroreload, null);
        loadingLayout = (RelativeLayout) view.findViewById(R.id.loadingLayout);
        reloadlayout = (RelativeLayout) view.findViewById(R.id.reloadlayout);
        setLoading();
        reloadlayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                reload.clicktoreload();
                setLoading();
            }
        });
    }

    public void setLoading() {
        reloadlayout.setVisibility(View.GONE);
        loadingLayout.setVisibility(View.VISIBLE);
    }

    public void setloadfail() {
        reloadlayout.setVisibility(View.VISIBLE);
        loadingLayout.setVisibility(View.GONE);
    }

    public interface Reload {
        public void clicktoreload();

    }
}
