package com.afish.syl.lib.api.config;

/**
 * 捷微标准默认接口类
 * Created by suyilong on 2015-08-07.
 */
public interface AFishInterfaceUrl {
    //http://192.168.100.119/SSHI/member/loadRandomNo?phoneno=13505009654
    String BASE_URL = "http://117.27.234.237:8080/hospital180/afish/";
    String URL = "http://117.27.234.237:8080/hospital180/";
    String LOAD_RANDOM = BASE_URL + "loadRandomNo?";
    String REGISTER = BASE_URL + "register?";
    String LOGIN = BASE_URL + "login?";
    String LOADMEMBER = BASE_URL + "loadMember?";
    String LOADSTORE = BASE_URL + "loadStore?";
    String LOADREGION = BASE_URL + "loadRegion?";
    String loadRegionAll = BASE_URL + "loadRegionAll?";
    String MODMEMBERS = BASE_URL + "modMembers?";
    //可单个字段修改用户信息
    String MODMEMBER = BASE_URL + "modMember?";
    String vaildApiKey = BASE_URL + "vaildApiKey?";
    String addReceipts = BASE_URL + "addReceipts?";

    String resetPwd = BASE_URL + "resetPwd?";

    String loadAdvert = BASE_URL + "loadAdvert?";
    String loadNews = BASE_URL + "loadNews?";
    String loadCategory = BASE_URL + "loadCategory?";
    String loadProduct = BASE_URL + "loadProduct?";

    String loadExchange = BASE_URL + "loadExchange?";
    String addBuy = BASE_URL + "addBuy?";
    String loadBuy = BASE_URL + "loadBuy?";
    String queryMember = BASE_URL + "queryMember?";
    String queryTemplate = BASE_URL + "queryTemplate?";
    String loadTemplate = BASE_URL + "loadTemplate?";


}
