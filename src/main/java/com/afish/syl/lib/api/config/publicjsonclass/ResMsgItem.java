package com.afish.syl.lib.api.config.publicjsonclass;

import com.afish.syl.lib.api.core.afishpublic.string.StrUtils;

/**
 * 捷微 标准JSON接收类
 * 作者：苏逸龙 on 2015-08-28 10:51
 * QQ：317616660
 */
public class ResMsgItem {

    /** Field 状态 */
    private int status;

    /** Field 文本消息 */
    private String msg;

    /** Field 验证钥匙 */
    private String key;

    /** Field 用户代码预留 */
    private String users_sn;

    /** Field 用户手机号预留 */
    private String phoneno;

    /** Field 主要数据JSON数组字符串 */
    private String item;

    /** Field JSON数组的字符串 */
    private int sum;

    /** Field 访问接口的时间 */
    private String time;

    /** Field description */
    private String exchange;

    /** Field description */
    String template;

    /** Field 状态,把0转为true 1转为false */
    boolean statused;

    /**
     * 转化之前的整个JSON
     */
    private String alljson;

    /**
     * MethodName:getExchange
     * Method description
     *
     *
     * @return
     */
    public String getExchange() {
        return exchange;
    }

    /**
     * MethodName:setExchange
     * Method description
     *
     *
     * @param exchange
     */
    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    /**
     * MethodName:getItem
     * Method description
     *
     *
     * @return
     */
    public String getItem() {
        return StrUtils.StrIfNull(item);
    }

    /**
     * MethodName:setItem
     * Method description
     *
     *
     * @param item
     */
    public void setItem(String item) {
        this.item = item;
    }

    /**
     * MethodName:getKey
     * Method description
     *
     *
     * @return
     */
    public String getKey() {
        return StrUtils.StrIfNull(key);
    }

    /**
     * MethodName:setKey
     * Method description
     *
     *
     * @param key
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * MethodName:getMsg
     * Method description
     *
     *
     * @return
     */
    public String getMsg() {
        return StrUtils.StrIfNull(msg);
    }

    /**
     * MethodName:setMsg
     * Method description
     *
     *
     * @param msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * MethodName:getPhoneno
     * Method description
     *
     *
     * @return
     */
    public String getPhoneno() {
        return StrUtils.StrIfNull(phoneno);
    }

    /**
     * MethodName:setPhoneno
     * Method description
     *
     *
     * @param phoneno
     */
    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    /**
     * MethodName:getStatus
     * Method description
     *
     *
     * @return
     */
    public int getStatus() {
        return status;
    }

    /**
     * MethodName:setStatus
     * Method description
     *
     *
     * @param status
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * MethodName:isStatused
     * Method description
     *
     *
     * @return
     */
    public boolean isStatused() {
        if (status == 0) {
            setStatused(true);
        } else {
            setStatused(false);
        }

        return statused;
    }

    /**
     * MethodName:setStatused
     * Method description
     *
     *
     * @param statused
     */
    public void setStatused(boolean statused) {
        this.statused = statused;
    }

    /**
     * MethodName:getSum
     * Method description
     *
     *
     * @return
     */
    public int getSum() {
        return sum;
    }

    /**
     * MethodName:setSum
     * Method description
     *
     *
     * @param sum
     */
    public void setSum(int sum) {
        this.sum = sum;
    }

    /**
     * MethodName:getTemplate
     * Method description
     *
     *
     * @return
     */
    public String getTemplate() {
        return template;
    }

    /**
     * MethodName:setTemplate
     * Method description
     *
     *
     * @param template
     */
    public void setTemplate(String template) {
        this.template = template;
    }

    /**
     * MethodName:getTime
     * Method description
     *
     *
     * @return
     */
    public String getTime() {
        return StrUtils.StrIfNull(time);
    }

    /**
     * MethodName:setTime
     * Method description
     *
     *
     * @param time
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * MethodName:getUsers_sn
     * Method description
     *
     *
     * @return
     */
    public String getUsers_sn() {
        return StrUtils.StrIfNull(users_sn);
    }

    /**
     * MethodName:setUsers_sn
     * Method description
     *
     *
     * @param users_sn
     */
    public void setUsers_sn(String users_sn) {
        this.users_sn = users_sn;
    }

    public String getAlljson() {
        return alljson;
    }

    public void setAlljson(String alljson) {
        this.alljson = alljson;
    }
}


