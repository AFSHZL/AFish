package com.afish.syl.lib.api.config.publicjsonclass;

/**
 * Created by Administrator on 2014-12-24.
 */
public class fjsx1Item {
    private String id;
    private String sx_sn;
    private String sx_name;
    private String note;
    private String action_url;
    private String p_id;
    private String img_1;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSx_sn() {
        return sx_sn;
    }

    public void setSx_sn(String sx_sn) {
        this.sx_sn = sx_sn;
    }

    public String getSx_name() {
        return sx_name;
    }

    public void setSx_name(String sx_name) {
        this.sx_name = sx_name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getAction_url() {
        return action_url;
    }

    public void setAction_url(String action_url) {
        this.action_url = action_url;
    }

    public String getP_id() {
        return p_id;
    }

    public void setP_id(String p_id) {
        this.p_id = p_id;
    }

    public String getImg_1() {
        return img_1;
    }

    public void setImg_1(String img_1) {
        this.img_1 = img_1;
    }
}
