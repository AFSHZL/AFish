package com.afish.syl.lib.api.afishlib.baidumaps;

public interface ILocationCallback {

	void callback(LocationInfo locationInfo);

}
