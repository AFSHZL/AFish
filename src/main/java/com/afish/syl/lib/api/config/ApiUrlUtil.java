package com.afish.syl.lib.api.config;


import android.content.Context;
import android.content.SharedPreferences;

import com.afish.syl.lib.api.core.afishpublic.string.StrUtils;


public class ApiUrlUtil {

    //艾佳接口 每个内部链接都加上skey
    public static String getApiUrl(Context context, String url, String paramString) {
        String apiUrlString = "";
        apiUrlString = url;
        //每个内部链接都加上skey
        SharedPreferences userLoginPreferences = context.getSharedPreferences("userlogin", 0);
        String skeyStr = userLoginPreferences.getString("skey", "");
        if (!StrUtils.isEmpty(skeyStr)) {
            apiUrlString +="&apikey=" + skeyStr;
        }
        if (paramString != null && !paramString.equals("")) {
            if (!paramString.substring(1, 1).equals("&")) {
                apiUrlString += "&" + paramString;
            }
        }

        return apiUrlString;

    }


}