package com.afish.syl.lib.api.config.publicjsonclass;

/**
 * Created by Administrator on 2015-04-22.
 */
public class ErrorMessageItem {
    private int error;
    private String error_msg;
    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getError_msg() {
        return error_msg;
    }

    public void setError_msg(String error_msg) {
        this.error_msg = error_msg;
    }
}
