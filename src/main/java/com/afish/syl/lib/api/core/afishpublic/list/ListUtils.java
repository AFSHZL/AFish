package com.afish.syl.lib.api.core.afishpublic.list;

import java.util.List;

/**
 * List操作类
 * Created by 苏逸龙 on 2015-08-18.
 */
public class ListUtils {

    /**
     * IsNotNull List是否不为空
     * @param list
     * @return
     */
    public static boolean IsNotNull(List<?> list) {
        return (list != null) && (list.size() > 0);
    }

    /**
     * IsNotNull List是否为空
     * @param list
     * @return
     */
    public static boolean IsNull(List<?> list) {
        return !IsNotNull(list);
    }
}


//~ Formatted by syl