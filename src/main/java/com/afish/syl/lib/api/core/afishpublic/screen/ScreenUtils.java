package com.afish.syl.lib.api.core.afishpublic.screen;

import android.app.Activity;

import android.util.DisplayMetrics;

/**
 * @author 苏逸龙     317616660@qq.com
 * @ClassName: ScreenUtil
 * @Description: TODO(获取屏幕的一些参数)
 * @date 2014-3-10 下午7:10:47
 */
public class ScreenUtils {

    /**
     * @Fields dm : TODO(实例话DisplayMetrics对象)
     */
    private static DisplayMetrics dm = new DisplayMetrics();

    static {}

    /**
     *
     * @param activity
     * @return Integer    返回类型
     * @Title: getScreenHeight
     * @Description: TODO(获取屏幕的高)
     */
    public static Integer getScreenHeight(Activity activity) {
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);

        return dm.heightPixels;
    }

    /**
     *
     * @param activity
     * @return Integer    返回类型
     * @Title: getScreenWidth
     * @Description: TODO(获取屏幕的宽)
     */
    public static Integer getScreenWidth(Activity activity) {
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);

        return dm.widthPixels;
    }
}


//~ Formatted by syl