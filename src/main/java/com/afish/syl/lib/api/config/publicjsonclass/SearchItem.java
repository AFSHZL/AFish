package com.afish.syl.lib.api.config.publicjsonclass;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015-03-08.
 */
public class SearchItem {

//    private dalei2Item dalei;

    private List<fjsx1Item> fjsx1 = new ArrayList<fjsx1Item>();

    private List<fjsx1Item> fjsx1mx = new ArrayList<fjsx1Item>();

    private List<BusItem> buslist = new ArrayList<BusItem>();


    public List<fjsx1Item> getFjsx1() {
        return fjsx1;
    }

    public void setFjsx1(List<fjsx1Item> fjsx1) {
        this.fjsx1 = fjsx1;
    }

    public List<fjsx1Item> getFjsx1mx() {
        return fjsx1mx;
    }

    public void setFjsx1mx(List<fjsx1Item> fjsx1mx) {
        this.fjsx1mx = fjsx1mx;
    }

    public List<BusItem> getBuslist() {
        return buslist;
    }

    public void setBuslist(List<BusItem> buslist) {
        this.buslist = buslist;
    }
}
