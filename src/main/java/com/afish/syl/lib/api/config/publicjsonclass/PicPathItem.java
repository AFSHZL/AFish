package com.afish.syl.lib.api.config.publicjsonclass;

public class PicPathItem {
    String halfPic;
    int halfPic_width;
    int halfPic_height;
    int picpath_width;
    int picpath_height;
    String picpath;
    String title;
    String url;

    public String getHalfPic() {
        return halfPic;
    }

    public void setHalfPic(String halfPic) {
        this.halfPic = halfPic;
    }

    public int getHalfPic_width() {
        return halfPic_width;
    }

    public void setHalfPic_width(int halfPic_width) {
        this.halfPic_width = halfPic_width;
    }

    public int getHalfPic_height() {
        return halfPic_height;
    }

    public void setHalfPic_height(int halfPic_height) {
        this.halfPic_height = halfPic_height;
    }

    public int getPicpath_width() {
        return picpath_width;
    }

    public void setPicpath_width(int picpath_width) {
        this.picpath_width = picpath_width;
    }

    public int getPicpath_height() {
        return picpath_height;
    }

    public void setPicpath_height(int picpath_height) {
        this.picpath_height = picpath_height;
    }

    public String getPicpath() {
        return picpath;
    }

    public void setPicpath(String picpath) {
        this.picpath = picpath;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
