package com.afish.syl.lib.api.core.http;

import net.tsz.afinal.FinalHttp;

/**
 * Created by suyilong on 2015-08-28.
 * 准备封装的HTTP类
 * CallBack 封装太过复杂,先简单封装一下
 */
public class AFishHttp extends FinalHttp {
    private static int iSocketTimeout = 10000;
    public AFishHttp() {
        //调用一下父类方法
        super();
        //设置一下超时时间为5秒
        this.configTimeout(iSocketTimeout);
    }

}
