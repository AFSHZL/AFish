package com.afish.syl.lib.api.component.viewcontroller;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.afish.syl.lib.R;
import com.afish.syl.lib.api.config.StaticStrUtils;
import com.afish.syl.lib.api.core.activity.baseactivity.AFishActivity;
import com.afish.syl.lib.api.core.afishpublic.string.StrUtils;


/**
 * 静态HTML快速加载
 */
public class HtmlWebViewActivity extends AFishActivity {

    String sHtmlUrl = "";
    String sTitle = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ///
        //隐藏导航栏
        setHideNavcationBar(true);
        setContentView(R.layout.activity_html_web_view);
        sHtmlUrl = getIntent().getStringExtra(StaticStrUtils.htmlurl);
        sTitle = getIntent().getStringExtra(StaticStrUtils.title);
        if (!StrUtils.isEmpty(sTitle)) {
            TextView title_tv = (TextView) findViewById(R.id.title_tv);
            title_tv.setText(sTitle);
        }
        if (!StrUtils.isEmpty(sHtmlUrl)) {
            WebView webView = (WebView) findViewById(R.id.webView);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setDomStorageEnabled(true);

            webView.setWebViewClient(new WebViewClient() {
                ProgressDialog prDialog;


                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    // TODO Auto-generated method stub
                    prDialog = ProgressDialog.show(HtmlWebViewActivity.this, null,
                            getString(R.string.jing_cai_nei_rong_ji_jiang_zhan_xian));
                    super.onPageStarted(view, url, favicon);
                }


                @Override
                public void onPageFinished(WebView view, String url) {
                    prDialog.dismiss();
                    super.onPageFinished(view, url);
                }
            });
            webView.loadUrl(sHtmlUrl);
        }

    }

    public void btnBackClick(View view) {
        this.finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }
}
