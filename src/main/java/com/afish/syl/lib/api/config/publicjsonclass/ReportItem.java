package com.afish.syl.lib.api.config.publicjsonclass;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2014-12-24.
 */
public class ReportItem {
    private String user_code;
    private String user_name;
    private String user_name2;
    private String qddm;
    private String from_type;
    private List<ReportRoleItem> role = new ArrayList<ReportRoleItem>();

    public String getUser_code() {
        return user_code;
    }

    public void setUser_code(String user_code) {
        this.user_code = user_code;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_name2() {
        return user_name2;
    }

    public void setUser_name2(String user_name2) {
        this.user_name2 = user_name2;
    }

    public String getQddm() {
        return qddm;
    }

    public void setQddm(String qddm) {
        this.qddm = qddm;
    }

    public String getFrom_type() {
        return from_type;
    }

    public void setFrom_type(String from_type) {
        this.from_type = from_type;
    }

    public List<ReportRoleItem> getRole() {
        return role;
    }

    public void setRole(List<ReportRoleItem> role) {
        this.role = role;
    }
}
