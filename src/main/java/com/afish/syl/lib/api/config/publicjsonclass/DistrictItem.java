package com.afish.syl.lib.api.config.publicjsonclass;

/**
 * Created by Administrator on 2014-12-24.
 */
public class DistrictItem {
    private String region_sn;
    private String region_name;
    private String region_outer_name;

    public String getRegion_sn() {
        return region_sn;
    }

    public void setRegion_sn(String region_sn) {
        this.region_sn = region_sn;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    public String getRegion_outer_name() {
        return region_outer_name;
    }

    public void setRegion_outer_name(String region_outer_name) {
        this.region_outer_name = region_outer_name;
    }
}
