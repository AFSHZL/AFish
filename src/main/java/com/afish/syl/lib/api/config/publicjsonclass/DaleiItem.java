package com.afish.syl.lib.api.config.publicjsonclass;

/**
 * Created by Administrator on 2015-01-03.
 */
public class DaleiItem {
    private String dldm;
    private String dlmc;
    private Integer xtmr;

    public String getDldm() {
        return dldm;
    }

    public void setDldm(String dldm) {
        this.dldm = dldm;
    }

    public String getDlmc() {
        return dlmc;
    }

    public void setDlmc(String dlmc) {
        this.dlmc = dlmc;
    }

    public Integer getXtmr() {
        return xtmr;
    }

    public void setXtmr(Integer xtmr) {
        this.xtmr = xtmr;
    }
}
