package com.afish.syl.lib.api.core.afishpublic.json;

import com.afish.syl.lib.api.config.publicjsonclass.ResMsgItem;
import com.afish.syl.lib.api.core.afishpublic.o.OUtils;

/**
 * 快速获取JSON注入类
 * 作者：苏逸龙 on 2015-08-28 10:51
 * QQ：317616660
 *
 */
public class AFishJSONCla {

    /**
     * MethodName:parseRes
     * Method description
     * 把JSON串转成ResMsgItem
     *
     * @param sJson
     *
     * @return
     */
    public static final ResMsgItem parseRes(String sJson) {
        //获取标准JSON转化成实体类
        ResMsgItem resMsgItem = AFishJSONUtils.parseObject(sJson, ResMsgItem.class);
        //并把这个标准的JSON的字符串赋值给alljson,方便需要的时候取
        if (OUtils.IsNotNull(resMsgItem)) {
            resMsgItem.setAlljson(sJson);
            return resMsgItem;
        }
        return null;
    }
}


//~ Formatted by syl