package com.afish.syl.lib.api.core.afishpublic.store;

import android.os.Environment;

import java.io.File;

/**
 * @author 苏逸龙 317616660@qq.com
 * @ClassName: StorePaths
 * @Description: TODO(SD存储操作类)
 * @date 2014-3-10 下午7:12:34
 */
public class StoreUtils {

    /**
     * @return String    返回类型
     * @Title: getPackRootPath
     * @Description: TODO(获取包的根目录)
     */
    public static String getPackRootPath() {
        File sdDir = null;

        // 获取跟目录
        sdDir = Environment.getRootDirectory();

        return sdDir.toString();
    }

    /**
     * @return String 返回类型
     * @Title: getSDPath
     * @Description: TODO(获取SD卡根目录)
     */
    public static String getSDPath() {
        String sSdDir = "";
        File   sdDir  = null;

        // 判断sd卡是否存在
        boolean sdCardExist = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);

        if (sdCardExist) {

            // 获取SD跟目录
            sdDir = Environment.getExternalStorageDirectory();
        }

        if (sdDir != null) {
            sSdDir = sdDir.toString();

            String s = sSdDir.substring(sSdDir.length() - 1, sSdDir.length());

            if (!s.equals("/")) {
                sSdDir += "/";
            }
        }

        return sSdDir;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
