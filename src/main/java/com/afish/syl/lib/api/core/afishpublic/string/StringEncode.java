package com.afish.syl.lib.api.core.afishpublic.string;

/**
 * ClassName: StringEncode
 * Class description
 *
 *
 * @version        1.0, 16/01/27
 * @author         syl    
 */
public class StringEncode {

    /** Field 用来加密的钥匙 */
    public static String key = "w";

    /**
     * 知识文档内容，通过金格控件保存问附件
     * 字符串解密
	 *
     * @param passStr
     *
     * @return
     */
    public static String strDecode(String passStr) {
        byte[] bypte = passStr.getBytes();
        byte   ps[]  = key.getBytes();
        int    pos   = 0;

        if (ps != null) {

            // 解密
            for (int i = 0; i < bypte.length; i++) {
                if (i == 1024) {
                    break;
                } else {
                    bypte[i] ^= ps[pos];
                }

                pos++;

                if (pos == ps.length) {
                    pos = 0;
                }
            }
        }

        passStr = new String(bypte);

        return passStr;
    }

    /**
     * MethodName:strEncode
     * Method description
     * 字符串加密
     *
     * @param source
     *
     * @return
     */
    public static String strEncode(String source) {
        byte bs[]  = source.getBytes();
        byte ps[]  = key.getBytes();
        int  pos   = 0;
        int  index = bs.length;

        if (ps != null) {
            for (int i = 0; i < index; i++) {
                bs[i] ^= ps[pos];
                pos++;

                if (pos == ps.length) {
                    pos = 0;
                }
            }
        }

        source = new String(bs);

        return source;
    }
}


//~ Formatted by syl