package com.afish.syl.lib.api.core.otto;

import com.afish.syl.lib.api.core.afishpublic.string.StrUtils;

/**
 * 广播Activity消息
 * 作者：苏逸龙 on 2015-09-01 20:39
 * QQ：317616660
 */
public class ActivityMsgEvent {

    Integer imsg;

    /** Field 消息 */
    String msg;

    /** Field 消息内容,可以为字符串或者其他 */
    String json;

    /** Field 消息Body */
    String body;

    /** Field 消息备注 */
    String note;

    /** Field 参数1 */
    String param;

    /** Field 参数2 */
    String param1;

    /** Field 参数3 */
    String param2;

    /** Field 也可以放一个对象 */
    Object object;

    /**
     * Constructs ...
     *
     */
    public ActivityMsgEvent() {}

    /**
     * Constructs ...
     * 只有msg的构造函数
     *
     * @param msg
     */
    public ActivityMsgEvent(String msg) {

        // TODO Auto-generated constructor stub
        this.msg = msg;
    }

    /**
     * Constructs ...
     * 有msg和json的构造函数
     *
     * @param msg
     * @param json
     */
    public ActivityMsgEvent(String msg, String json) {

        // TODO Auto-generated constructor stub
        this.msg  = msg;
        this.json = json;
    }

    /**
     * MethodName:getBody
     * Method description
     *
     *
     * @return
     */
    public String getBody() {
        return StrUtils.IsNull(body);
    }

    /**
     * MethodName:setBody
     * Method description
     *
     *
     * @param body
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * MethodName:getImsg
     * Method description
     *
     *
     * @return
     */
    public Integer getImsg() {
        if (imsg == null) {
            return -1;
        } else {
            return imsg;
        }
    }

    /**
     * MethodName:setImsg
     * Method description
     *
     *
     * @param imsg
     */
    public void setImsg(Integer imsg) {
        this.imsg = imsg;
    }

    /**
     * MethodName:getJson
     * Method description
     *
     *
     * @return
     */
    public String getJson() {
        return json;
    }

    /**
     * MethodName:setJson
     * Method description
     *
     *
     * @param json
     */
    public void setJson(String json) {
        this.json = json;
    }

    /**
     * MethodName:getMsg
     * Method description
     *
     *
     * @return
     */
    public String getMsg() {
        return StrUtils.IsNull(msg);
    }

    /**
     * MethodName:setMsg
     * Method description
     *
     *
     * @param msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * MethodName:getNote
     * Method description
     *
     *
     * @return
     */
    public String getNote() {
        return StrUtils.IsNull(note);
    }

    /**
     * MethodName:setNote
     * Method description
     *
     *
     * @param note
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * MethodName:getObject
     * Method description
     *
     *
     * @return
     */
    public Object getObject() {
        return object;
    }

    /**
     * MethodName:setObject
     * Method description
     *
     *
     * @param object
     */
    public void setObject(Object object) {
        this.object = object;
    }

    /**
     * MethodName:getParam
     * Method description
     *
     *
     * @return
     */
    public String getParam() {
        return StrUtils.IsNull(param);
    }

    /**
     * MethodName:setParam
     * Method description
     *
     *
     * @param param
     */
    public void setParam(String param) {
        this.param = param;
    }

    /**
     * MethodName:getParam1
     * Method description
     *
     *
     * @return
     */
    public String getParam1() {
        return StrUtils.IsNull(param1);
    }

    /**
     * MethodName:setParam1
     * Method description
     *
     *
     * @param param1
     */
    public void setParam1(String param1) {
        this.param1 = param1;
    }

    /**
     * MethodName:getParam2
     * Method description
     *
     *
     * @return
     */
    public String getParam2() {
        return StrUtils.IsNull(param2);
    }

    /**
     * MethodName:setParam2
     * Method description
     *
     *
     * @param param2
     */
    public void setParam2(String param2) {
        this.param2 = param2;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
