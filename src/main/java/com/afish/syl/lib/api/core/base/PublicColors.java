package com.afish.syl.lib.api.core.base;

/**
 * ClassName: PublicColors
 * Class description
 * 公共颜色
 *
 *
 * @version        1.0, 16/01/27
 * @author         syl    
 */
public class PublicColors {

    /** Field 整个Bar颜色 */
    private static int navicationBarColorRID = 0;

    /** Field 文字的颜色 */
    private static int textColorRID = 0;

    /** Field 全自定义Bar */
    private static int layoutRID = 0;

    /** Field 返回按钮图标 */
    private static int backBtnRID = 0;

    /**
     * MethodName:getBackBtnRID
     * Method description
     *
     *
     * @return
     */
    public static int getBackBtnRID() {
        return backBtnRID;
    }

    /**
     * MethodName:setBackBtnRID
     * Method description
     *
     *
     * @param backBtnRID
     */
    public static void setBackBtnRID(int backBtnRID) {
        PublicColors.backBtnRID = backBtnRID;
    }

    /**
     * MethodName:getLayoutRID
     * Method description
     *
     *
     * @return
     */
    public static int getLayoutRID() {
        return layoutRID;
    }

    /**
     * MethodName:setLayoutRID
     * Method description
     *
     *
     * @param layoutRID
     */
    public static void setLayoutRID(int layoutRID) {
        PublicColors.layoutRID = layoutRID;
    }

    /**
     * MethodName:getNavicationBarColorRID
     * Method description
     *
     *
     * @return
     */
    public static int getNavicationBarColorRID() {
        return navicationBarColorRID;
    }

    /**
     * MethodName:setNavicationBarColorRID
     * Method description
     *
     *
     * @param navicationBarColorRID
     */
    public static void setNavicationBarColorRID(int navicationBarColorRID) {
        PublicColors.navicationBarColorRID = navicationBarColorRID;
    }

    /**
     * MethodName:getTextColorRID
     * Method description
     *
     *
     * @return
     */
    public static int getTextColorRID() {
        return textColorRID;
    }

    /**
     * MethodName:setTextColorRID
     * Method description
     *
     *
     * @param textColorRID
     */
    public static void setTextColorRID(int textColorRID) {
        PublicColors.textColorRID = textColorRID;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
