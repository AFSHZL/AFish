package com.afish.syl.lib.api.config.publicjsonclass;

/**
 * Created by Administrator on 2014-12-23.
 */
//":[{"id":"40","name":"系统消息","p_id":"111006","note":"2014-12-12 12:15:45查询的进销存数据已经生成","is_send":"0","send_num":"26","msg_type":"0","add_time":"2014-12-12 12:15:45"},{"
public class SendMsgItem {
    private Integer id;
    private String name;
    private Integer p_id;
    private String note;
    private Integer is_send;
    private Integer send_num;
    private Integer msg_type;
    private String add_time;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getP_id() {
        return p_id;
    }

    public void setP_id(Integer p_id) {
        this.p_id = p_id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getIs_send() {
        return is_send;
    }

    public void setIs_send(Integer is_send) {
        this.is_send = is_send;
    }

    public Integer getSend_num() {
        return send_num;
    }

    public void setSend_num(Integer send_num) {
        this.send_num = send_num;
    }

    public Integer getMsg_type() {
        return msg_type;
    }

    public void setMsg_type(Integer msg_type) {
        this.msg_type = msg_type;
    }

    public String getAdd_time() {
        return add_time;
    }

    public void setAdd_time(String add_time) {
        this.add_time = add_time;
    }
}
