package com.afish.syl.lib.api.core.afishpublic.sim;

import android.content.Context;
import android.content.Intent;

import android.net.Uri;

import android.telephony.TelephonyManager;

/**
 * class name：SIMCardInfo<BR>
 * class description：读取Sim卡信息<BR>
 * PS： 必须在加入各种权限 <BR>
 * Date:2012-3-12<BR>
 *
 * @author CODYY)peijiangping
 * @version 1.00
 */
public class SIMCardUtils {

    /**
     * TelephonyManager提供设备上获取通讯服务信息的入口。 应用程序可以使用这个类方法确定的电信服务商和国家 以及某些类型的用户访问信息。
     * 应用程序也可以注册一个监听器到电话收状态的变化。不需要直接实例化这个类
     * 使用Context.getSystemService(Context.TELEPHONY_SERVICE)来获取这个类的实例。
     */
    private TelephonyManager telephonyManager;

    /**
     * 国际移动用户识别码
     */
    private String IMSI;

    /**
     * Constructs ...
     *
     * @param context
     */
    public SIMCardUtils(Context context) {
        telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
    }

    /**
     * MethodName:GetMobileDeviceId
     * Method description
     * 获取手机设备ID
     * @return
     */
    public String GetMobileDeviceId() {
        String deviceIdString = telephonyManager.getDeviceId();

        return deviceIdString;
    }

    /**
     * MethodName:GetMobileSubscribreId
     * Method description
     * 获取手机ID
     * @return
     */
    public String GetMobileSubscribreId() {
        IMSI = telephonyManager.getSubscriberId();

        return IMSI;
    }

    /**
     * MethodName:callIphone
     * Method description
     * 拨打电话
     *
     * @param context
     * @param number
     */
    public static void callIphone(Context context, String number) {

        // 用intent启动拨打电话
        // 调用拨号程序，手工拨出。
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel://" + number));

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    /**
     * MethodName:callIphone2
     * Method description
     * 拨打手机电话(直接拨打电话方式)
     *
     * @param context
     * @param number
     */
    public static void callIphone2(Context context, String number) {

        // 用intent启动拨打电话
        // 直接拨号
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel://" + number));

        context.startActivity(intent);
    }

    /**
     * Role:获取当前设置的电话号码
     * <BR>Date:2012-3-12
     * <BR>@author CODYY)peijiangping
     *
     * @return
     */
    public String getNativePhoneNumber() {
        String NativePhoneNumber = null;

        NativePhoneNumber = telephonyManager.getLine1Number();

        return NativePhoneNumber;
    }

    /**
     * Role:Telecom service providers获取手机服务商信息 <BR>
     * 需要加入权限<uses-permission
     * android:name="android.permission.READ_PHONE_STATE"/> <BR>
     * Date:2012-3-12 <BR>
     *
     * @return
     * @author CODYY)peijiangping
     */
    public String getProvidersName() {
        String ProvidersName = null;

        // 返回唯一的用户ID;就是这张卡的编号神马的
        IMSI = telephonyManager.getSubscriberId();

        // IMSI号前面3位460是国家，紧接着后面2位00 02是中国移动，01是中国联通，03是中国电信。
        System.out.println(IMSI);

        if (IMSI.startsWith("46000") || IMSI.startsWith("46002")) {
            ProvidersName = "中国移动";
        } else if (IMSI.startsWith("46001")) {
            ProvidersName = "中国联通";
        } else if (IMSI.startsWith("46003")) {
            ProvidersName = "中国电信";
        }

        return ProvidersName;
    }
}


//~ Formatted by syl