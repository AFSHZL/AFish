package com.afish.syl.lib.api.core.afishpublic.string;

import java.io.UnsupportedEncodingException;

import java.net.URLDecoder;

/**
 * 快速Url解码类
 * 作者：苏逸龙 on 2015-08-28 10:51
 * QQ：317616660
 *
 */
public class StrURLDecoder {

    /**
     * MethodName:decode
     * Method description
     * URL解码
     *
     * @param s
     *
     * @return
     */
    public static String decode(String s) {
        try {
            return StrUtils.IsNull(URLDecoder.decode(s, "utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        //如果是错误返回""
        return "";
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
