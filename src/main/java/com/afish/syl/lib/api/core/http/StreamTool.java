package com.afish.syl.lib.api.core.http;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * ClassName: StreamTool
 * Class description
 * 内存流工具类
 *
 * @version        1.0, 16/01/27
 * @author         syl    
 */
public class StreamTool {

    /**
     * 从输入流中获取数据
     *
     * @param inStream 输入流
     * @return byte[]
     * @throws Exception
     */
    public static byte[] ReadInputSream(InputStream inStream) throws Exception {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        byte[]                buffer    = new byte[1024];
        int                   len       = 0;

        while ((len = inStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, len);
        }

        inStream.close();

        return outStream.toByteArray();
    }

    /**
     * MethodName:getImage
     * Method description
     * 获取图片字节
     *
     * @param path
     *
     * @return
     *
     * @throws Exception
     */
    public static byte[] getImage(String path) throws Exception {
        URL               url  = new URL(path);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        conn.setRequestMethod("GET");
        conn.setConnectTimeout(5 * 1000);

        InputStream inStream = conn.getInputStream();    // 通过输入流获取图片数据

        return ReadInputSream(inStream);    // 得到图片的二进制数据
    }
}


//~ Formatted by syl