package com.afish.syl.lib.api.core.afishpublic.time;

import java.util.Date;
import java.util.TimeZone;

/**
 * ClassName: TimeZoneUtils
 * Class description
 * 时间空间单元
 *
 * @version        1.0, 16/01/27
 * @author         syl    
 */
public class TimeZoneUtils {

    /**
     * 根据不同时区，转换时间 2014年7月31日
     *
     *
     * @param date
     * @param oldZone
     * @param newZone
     * @return
     */
    public static Date transformTime(Date date, TimeZone oldZone, TimeZone newZone) {
        Date finalDate = null;

        if (date != null) {
            int timeOffset = oldZone.getOffset(date.getTime()) - newZone.getOffset(date.getTime());

            finalDate = new Date(date.getTime() - timeOffset);
        }

        return finalDate;
    }

    /**
     * 判断用户的设备时区是否为东八区（中国） 2014年7月31日
     *
     * @return
     */
    public static boolean isInEasternEightZones() {
        boolean defaultVaule = true;

        defaultVaule = TimeZone.getDefault() == TimeZone.getTimeZone("GMT+08");

        return defaultVaule;
    }
}


//~ Formatted by syl