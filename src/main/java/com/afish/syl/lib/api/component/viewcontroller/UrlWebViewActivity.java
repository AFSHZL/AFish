package com.afish.syl.lib.api.component.viewcontroller;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.afish.syl.lib.R;
import com.afish.syl.lib.api.core.activity.baseactivity.AFishActivity;
import com.afish.syl.lib.api.core.afishpublic.string.StrUtils;


public class UrlWebViewActivity extends AFishActivity {
    String sHtmlUrl = "";
    String sTitle = "";
    WebView mWebView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //隐藏导航栏
        setHideNavcationBar(true);
        setContentView(R.layout.activity_html_web_view);
        sHtmlUrl = getIntent().getStringExtra("htmlurl");
        sTitle = getIntent().getStringExtra("title");
        if (!StrUtils.isEmpty(sTitle)) {
            TextView title_tv = (TextView) findViewById(R.id.title_tv);
            title_tv.setText(sTitle);
        }
        if (!StrUtils.isEmpty(sHtmlUrl)) {
            mWebView = (WebView) findViewById(R.id.webView);
            mWebView.getSettings().setJavaScriptEnabled(true);
            mWebView.getSettings().setDomStorageEnabled(true);
            mWebView.setWebViewClient(new WebViewClient() {
//                ProgressDialog prDialog;


                @Override
                public void onPageStarted(WebView view, String url,
                                          Bitmap favicon) {
                    // TODO Auto-generated method stub
                    showLoading(getString(R.string.jing_cai_nei_rong_ji_jiang_zhan_xian));
//                    prDialog = ProgressDialog.show(UrlWebViewActivity.this, null,
//                            "精彩内容即将展现,请您稍等...");
//                    prDialog.setCancelable(true);
                    super.onPageStarted(view, url, favicon);
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    hideLoading();
                    super.onPageFinished(view, url);
                }
            });
            mWebView.loadUrl(sHtmlUrl);
        }

    }

    // 浏览网页历史记录
    // goBack()和goForward()
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
            mWebView.goBack();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }



    public void btnGoBackClick(View view) {
        if (mWebView.canGoBack()) {
            mWebView.goBack();
            return ;
        }
        this.finish();
    }

    public void btnBackClick(View view) {
        this.finish();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

}
