package com.afish.syl.lib.api.config.publicjsonclass;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015-03-08.
 */
public class BusItem {
    private String id;
    private String business_sn;
    private String business_name;
    private String reg_time;
    private String qq;
    private String tel;
    private String moblie_phone;
    private String reg_type;
    private String is_special;
    private float bs_rank;
    private String is_warn;
    private String address;
    private String fw_fjsx1;
    private String sample_note;
    private String work_starttime;
    private String note;
    private String work_endtime;
    private String user_name;
    private String province;
    private String city;
    private String dis;
    private String pinglun;
    private String image_url;
    private String city_name;
    private String dis_name;
    private List<PinglunItem> pinglun_list = new ArrayList<PinglunItem>();

    public List<PinglunItem> getPinglun_list() {
        return pinglun_list;
    }

    public void setPinglun_list(List<PinglunItem> pinglun_list) {
        this.pinglun_list = pinglun_list;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getDis_name() {
        return dis_name;
    }

    public void setDis_name(String dis_name) {
        this.dis_name = dis_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBusiness_sn() {
        return business_sn;
    }

    public void setBusiness_sn(String business_sn) {
        this.business_sn = business_sn;
    }

    public String getBusiness_name() {
        return business_name;
    }

    public void setBusiness_name(String business_name) {
        this.business_name = business_name;
    }

    public String getReg_time() {
        return reg_time;
    }

    public void setReg_time(String reg_time) {
        this.reg_time = reg_time;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getMoblie_phone() {
        return moblie_phone;
    }

    public void setMoblie_phone(String moblie_phone) {
        this.moblie_phone = moblie_phone;
    }

    public String getReg_type() {
        return reg_type;
    }

    public void setReg_type(String reg_type) {
        this.reg_type = reg_type;
    }

    public String getIs_special() {
        return is_special;
    }

    public void setIs_special(String is_special) {
        this.is_special = is_special;
    }

    public float getBs_rank() {
        return bs_rank;
    }

    public void setBs_rank(float bs_rank) {
        this.bs_rank = bs_rank;
    }

    public String getIs_warn() {
        return is_warn;
    }

    public void setIs_warn(String is_warn) {
        this.is_warn = is_warn;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFw_fjsx1() {
        return fw_fjsx1;
    }

    public void setFw_fjsx1(String fw_fjsx1) {
        this.fw_fjsx1 = fw_fjsx1;
    }

    public String getSample_note() {
        return sample_note;
    }

    public void setSample_note(String sample_note) {
        this.sample_note = sample_note;
    }

    public String getWork_starttime() {
        return work_starttime;
    }

    public void setWork_starttime(String work_starttime) {
        this.work_starttime = work_starttime;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getWork_endtime() {
        return work_endtime;
    }

    public void setWork_endtime(String work_endtime) {
        this.work_endtime = work_endtime;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDis() {
        return dis;
    }

    public void setDis(String dis) {
        this.dis = dis;
    }

    public String getPinglun() {
        return pinglun;
    }

    public void setPinglun(String pinglun) {
        this.pinglun = pinglun;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
}
