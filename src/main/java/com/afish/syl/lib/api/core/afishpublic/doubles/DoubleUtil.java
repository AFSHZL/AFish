package com.afish.syl.lib.api.core.afishpublic.doubles;

/**
 * ClassName: DoubleUtil
 * Class description
 * 双精度单元
 *
 * @version        1.0, 16/01/27
 * @author         syl    
 */
public class DoubleUtil {

    /**
     * MethodName:dtoStr
     * Method description
     * 双精度toStr
     *
     * @param dtr
     *
     * @return
     */
    public static String dtoStr(Double dtr) {
        String str = "";

        if (null != dtr) {
            str = dtr + "";
        }

        return str;
    }
}


//~ Formatted by syl