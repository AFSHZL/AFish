package com.afish.syl.lib.api.core.log;

import android.text.TextUtils;

import android.util.Log;

/**
 * AFishLog Debug调试Log封装类
 * 作者：苏逸龙 on 2015-08-31 09:44
 * QQ：317616660
 */

/**
 * AFishLog:
 * 1 只输出等级大于等于LEVEL的日志
 *   所以在开发和产品发布后通过修改LEVEL来选择性输出日志.
 *   当LEVEL=NOTHING则屏蔽了所有的日志.
 * 2 v,d,i,w,e均对应两个方法.
 *   若不设置TAG或者TAG为空则为设置默认TAG
 *
 */

/**
 * 1、Log.v 的调试颜色为黑色的，任何消息都会输出，这里的v代表verbose啰嗦的意思，平时使用就是Log.v("","");
 *
 * 2、Log.d的输出颜色是蓝色的，仅输出debug调试的意思，但他会输出上层的信息，过滤起来可以通过DDMS的Logcat标签来选择.
 *
 * 3、Log.i的输出为绿色，一般提示性的消息information，它不会输出Log.v和Log.d的信息，但会显示i、w和e的信息
 *
 * 4、Log.w的意思为橙色，可以看作为warning警告，一般需要我们注意优化Android代码，同时选择它后还会输出Log.e的信息。
 *
 * 5、Log.e为红色，可以想到error错误，这里仅显示红色的错误信息，这些错误就需要我们认真的分析，查看栈的信息了。
 */
public class AFishLog {

    /** Field description */
    public static final int VERBOSE = 1;

    /** Field description */
    public static final int DEBUG = 2;

    /** Field description */
    public static final int INFO = 3;

    /** Field description */
    public static final int WARN = 4;

    /** Field description */
    public static final int ERROR = 5;

    /** Field description */
    public static final int NOTHING = 6;

    /** Field description */
    public static final int LEVEL = VERBOSE;

    /** Field description */
    public static final String SEPARATOR = ",";

    /**
     * Log.d的输出颜色是蓝色的，仅输出debug调试的意思，但他会输出上层的信息，过滤起来可以通过DDMS的Logcat标签来选择.
     * @param message
     */
    public static void d(String message) {
        if (LEVEL <= DEBUG) {
            StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[3];
            String            tag               = getDefaultTag(stackTraceElement);

            Log.d(tag, getLogInfo(stackTraceElement) + message);
        }
    }

    /**
     * Log.d的输出颜色是蓝色的，仅输出debug调试的意思，但他会输出上层的信息，过滤起来可以通过DDMS的Logcat标签来选择.
     * @param tag
     * @param message
     */
    public static void d(String tag, String message) {
        if (LEVEL <= DEBUG) {
            StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[3];

            if (TextUtils.isEmpty(tag)) {
                tag = getDefaultTag(stackTraceElement);
            }

            Log.d(tag, getLogInfo(stackTraceElement) + message);
        }
    }

    /**
     * Log.e为红色，可以想到error错误，这里仅显示红色的错误信息，这些错误就需要我们认真的分析，查看栈的信息了。
     * @param message
     */
    public static void e(String message) {
        if (LEVEL <= ERROR) {
            StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[3];
            String            tag               = getDefaultTag(stackTraceElement);

            Log.e(tag, getLogInfo(stackTraceElement) + message);
        }
    }

    /**
     * Log.e为红色，可以想到error错误，这里仅显示红色的错误信息，这些错误就需要我们认真的分析，查看栈的信息了。
     * @param tag
     * @param message
     */
    public static void e(String tag, String message) {
        if (LEVEL <= ERROR) {
            StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[3];

            if (TextUtils.isEmpty(tag)) {
                tag = getDefaultTag(stackTraceElement);
            }

            Log.e(tag, getLogInfo(stackTraceElement) + message);
        }
    }

    /**
     * Log.i的输出为绿色，一般提示性的消息information，它不会输出Log.v和Log.d的信息，但会显示i、w和e的信息
     * @param message
     */
    public static void i(String message) {
        if (LEVEL <= INFO) {
            StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[3];
            String            tag               = getDefaultTag(stackTraceElement);

            Log.i(tag, getLogInfo(stackTraceElement) + message);
        }
    }

    /**
     * Log.i的输出为绿色，一般提示性的消息information，它不会输出Log.v和Log.d的信息，但会显示i、w和e的信息
     * @param tag
     * @param message
     */
    public static void i(String tag, String message) {
        if (LEVEL <= INFO) {
            StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[3];

            if (TextUtils.isEmpty(tag)) {
                tag = getDefaultTag(stackTraceElement);
            }

            Log.i(tag, getLogInfo(stackTraceElement) + message);
        }
    }

    /**
     * Log.v 的调试颜色为黑色的，任何消息都会输出，这里的v代表verbose啰嗦的意思，平时使用就是Log.v("","");
     * @param message
     */
    public static void v(String message) {
        if (LEVEL <= VERBOSE) {
            StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[3];
            String            tag               = getDefaultTag(stackTraceElement);

            Log.v(tag, getLogInfo(stackTraceElement) + message);
        }
    }

    /**
     * Log.v 的调试颜色为黑色的，任何消息都会输出，这里的v代表verbose啰嗦的意思，平时使用就是Log.v("","");
     * @param tag
     * @param message
     */
    public static void v(String tag, String message) {
        if (LEVEL <= VERBOSE) {
            StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[3];

            if (TextUtils.isEmpty(tag)) {
                tag = getDefaultTag(stackTraceElement);
            }

            Log.v(tag, getLogInfo(stackTraceElement) + message);
        }
    }

    /**
     * Log.w的意思为橙色，可以看作为warning警告，一般需要我们注意优化Android代码，同时选择它后还会输出Log.e的信息。
     * @param message
     */
    public static void w(String message) {
        if (LEVEL <= WARN) {
            StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[3];
            String            tag               = getDefaultTag(stackTraceElement);

            Log.w(tag, getLogInfo(stackTraceElement) + message);
        }
    }

    /**
     * Log.w的意思为橙色，可以看作为warning警告，一般需要我们注意优化Android代码，同时选择它后还会输出Log.e的信息。
     * @param tag
     * @param message
     */
    public static void w(String tag, String message) {
        if (LEVEL <= WARN) {
            StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[3];

            if (TextUtils.isEmpty(tag)) {
                tag = getDefaultTag(stackTraceElement);
            }

            Log.w(tag, getLogInfo(stackTraceElement) + message);
        }
    }

    /**
     * 获取默认的TAG名称.
     * 比如在MainActivity.java中调用了日志输出.
     * 则TAG为MainActivity
     *
     * @param stackTraceElement
     *
     * @return
     */
    public static String getDefaultTag(StackTraceElement stackTraceElement) {
        String fileName      = stackTraceElement.getFileName();
        String stringArray[] = fileName.split("\\.");
        String tag           = stringArray[0];

        return tag;
    }

    /**
     * 输出日志所包含的信息
     * @param stackTraceElement
     * @return
     */
    public static String getLogInfo(StackTraceElement stackTraceElement) {
        StringBuilder logInfoStringBuilder = new StringBuilder();

        // 获取线程名
        String threadName = Thread.currentThread().getName();

        // 获取线程ID
        long threadID = Thread.currentThread().getId();

        // 获取文件名.即xxx.java
        String fileName = stackTraceElement.getFileName();

        // 获取类名.即包名+类名
        String className = stackTraceElement.getClassName();

        // 获取方法名称
        String methodName = stackTraceElement.getMethodName();

        // 获取生日输出行数
        int lineNumber = stackTraceElement.getLineNumber();

        logInfoStringBuilder.append("[ ");
        logInfoStringBuilder.append("threadID=" + threadID).append(SEPARATOR);
        logInfoStringBuilder.append("threadName=" + threadName).append(SEPARATOR);
        logInfoStringBuilder.append("fileName=" + fileName).append(SEPARATOR);
        logInfoStringBuilder.append("className=" + className).append(SEPARATOR);
        logInfoStringBuilder.append("methodName=" + methodName).append(SEPARATOR);
        logInfoStringBuilder.append("lineNumber=" + lineNumber);
        logInfoStringBuilder.append(" ] ");

        return logInfoStringBuilder.toString();
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
