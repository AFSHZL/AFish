package com.afish.syl.lib.api.core.afishpublic.json;

import com.alibaba.fastjson.JSON;
import com.afish.syl.lib.api.core.afishpublic.string.StrUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author suyilong     317616660@qq.com
 * @ClassName: StringToJSONObjectUtil
 * @Description: TODO(把字符串转化成JSONArray)
 * @date 2014-3-15 下午2:17:47
 */
public class AFishJSONUtils {

    /**
     * @return JSONArray    返回类型
     * @Title: StrToJson
     * @Description: TODO(把字符串转化成JSONArray)
     */
    public static JSONArray StrToJson(String jString) {
        JSONArray jsonItems = null;

        if ((jString != null) &&!jString.equals("")) {

            // 解决2.x JSON无法加载问题 2.x没有对下载的JSON： UTF8 BOM头
            String  jsString   = jString.replace("\ufeff", "");
            Integer sumInteger = 0;

            try {
                JSONObject jsonObject = new JSONObject(jsString);

                sumInteger = jsonObject.getInt("sum");

                if (sumInteger > 0) {

                    // 得到主item的所有数据
                    jsonItems = (JSONArray) jsonObject.get("item");
                }
            } catch (JSONException e) {

                // TODO Auto-generated catch block
                jsonItems = null;
            }
        }

        return jsonItems;
    }

    /**
     * MethodName:checkJson
     * Method description
     * 验证JSON是否标准
     *
     * @param sText
     *
     * @return
     */
    public static boolean checkJson(String sText) {
        try {
            JSONObject jsonObject = new JSONObject(sText);

            return true;
        } catch (JSONException e) {
            return false;
        }
    }

    // 经过处理的JSON类

    /**
     * MethodName:parseObject
     * Method description
     *
     *
     * @param sText
     * @param clazz
     * @param <T>
     *
     * @return
     */
    public static final <T> T parseObject(String sText, Class<T> clazz) {
        if (!StrUtils.isEmpty(sText) && checkJson(sText)) {
            sText = AFishJSONUtils.verifyJSON(sText);

            return JSON.parseObject(sText, clazz);
        } else {
            return null;
        }
    }

    /**
     * 验证并修改JSON
     * 很多JSON都有BOM头,无法转化成JSONObject
     * @param sJson
     * @return
     */
    public static String verifyJSON(String sJson) {
        return sJson.replace("\ufeff", "");
    }

    /**
     * 获取vo json list
     * @param jsonString
     * @param cls
     * @return
     */
    public static List getParseArray(String jsonString, Class cls) {
        jsonString = AFishJSONUtils.verifyJSON(jsonString);

        List list = new ArrayList();

        if (StrUtils.IsNotEmpty(jsonString)) {
            try {
                list = JSON.parseArray(jsonString, cls);
            } catch (Exception e) {
                System.out.print(e.getMessage());
            }
        }

        return list;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
