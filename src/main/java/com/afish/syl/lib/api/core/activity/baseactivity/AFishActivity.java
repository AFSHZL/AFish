package com.afish.syl.lib.api.core.activity.baseactivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PersistableBundle;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afish.syl.lib.R;
import com.afish.syl.lib.api.component.dialog.SFProgrssDialog;
import com.afish.syl.lib.api.component.viewcontroller.UrlWebViewActivity;
import com.afish.syl.lib.api.config.StaticStrUtils;
import com.afish.syl.lib.api.config.publicjsonclass.BaseItem;
import com.afish.syl.lib.api.config.publicjsonclass.ResMsgItem;
import com.afish.syl.lib.api.core.activity.activitytransition.ActivityAnimator;
import com.afish.syl.lib.api.core.base.AFishApplication;
import com.afish.syl.lib.api.core.base.PublicColors;
import com.afish.syl.lib.api.core.afishpublic.json.AFishJSONCla;
import com.afish.syl.lib.api.core.afishpublic.o.OUtils;
import com.afish.syl.lib.api.core.afishpublic.string.StrUtils;
import com.afish.syl.lib.api.core.afishutil.NumberHelper;
import com.afish.syl.lib.api.core.log.AFishLog;
import com.afish.syl.lib.api.core.otto.ActivityMsgEvent;
import com.afish.syl.lib.api.core.otto.BaiduPushEvent;
import com.afish.syl.lib.api.core.otto.OttoBus;
import com.afish.syl.lib.api.core.otto.PushExtraEvent;
import com.afish.syl.lib.api.core.toast.AFishToast;

import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.http.AjaxCallBack;
import net.tsz.afinal.http.AjaxParams;

import de.keyboardsurfer.android.widget.crouton.Configuration;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * jw快速开发框架
 * 集成快速Base Activity
 * by 苏逸龙
 */
public class AFishActivity extends Activity {
    /**
     * 网络延迟
     */
    private static int iSocketTimeout = 8000;
    /**
     * 上下文
     */
    private Context context;
    /**
     * 上下文
     */
    private Context my;
    /**
     * This 私有的 继承的使用它自己的This 这里仅代表父类
     */
    public AFishActivity self;
    /**
     * 偶尔需要的传值,尝试用OTTO代替
     */
    public Intent intent;
    /**
     * 简易的正在加载中Dialog
     */
    public ProgressDialog pDialog;
    /**
     * 核心Http 请求创建,每个页面都有可能访问网络
     */
    public FinalHttp jwHttp;
    /**
     * 切换动画效果
     */
    private String[] _animationList = {"fade", "flipHorizontal", "flipVertical",
            "disappearTopLeft", "appearTopLeft", "appearBottomRight",
            "disappearBottomRight", "unzoom"};
    /**
     * 默认动画效果
     */
    private int animationPos = 0;
    /**
     * 公共Application
     */
    AFishApplication app;
    /**
     * 动态存储,每个Activity一个唯一值
     * 为了内容比较多的本地缓存预留
     */
    SharedPreferences sf;
    /**
     * 返回按钮
     */
    private Button btnBack;
    /**
     * 导航栏标题
     */
    private TextView title_tv;

    /**
     * 动画跳动加载--已被淘汰
     */
    private RelativeLayout loadingLayout;
    /**
     * 页面为空重新加载
     */
    private RelativeLayout reloadlayout;
    /**
     * 隐藏Action
     */
    private TranslateAnimation mShowAction, mHiddenAction;
    /**
     * Activity动态配置信息
     */
    private Configuration configuration;
    /**
     * 导航栏
     */
    private FrameLayout navcationBar;
    /**
     * 是否隐藏导航栏
     */
    boolean hideNavcationBar = false;
    /**
     * 是否隐藏导航栏
     */
    boolean hideBack = false;
    /**
     * 默认导航栏颜色资源值
     */
    int iNavcationBarColor = 0;

    /**
     * 菜单
     */
    private LinearLayout menu;

    /**
     * 构造函数
     */
    public AFishActivity() {
    }

    /**
     * 设置默认的View For layoutResID
     *
     * @param layoutResID
     */
    public void setContentView(int layoutResID) {
        LayoutInflater inflater = LayoutInflater.from(this);
        //通过Res ID 载入View
        View view = inflater.inflate(layoutResID, null);
        //添加导航栏到view
        View root = initNavcationBar(view);
        //重新设置
        super.setContentView(root);
        initBase();
    }

    /**
     * 设置默认的View
     *
     * @param view
     * @param params
     */
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        //与上相同
        View root = initNavcationBar(view);
        super.setContentView(root, params);
        initBase();
    }

    /**
     * 设置默认的View
     *
     * @param view
     */
    public void setContentView(View view) {
        //与上相同
        View root = initNavcationBar(view);
        super.setContentView(root);
        initBase();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //子类onCreate创建之前运行的代码
        initCreate();
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        //子类onCreate创建之前运行的代码
        initCreate();
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    /**
     * 在OnCreate 之前的初始时
     * 子类的super.onCreate时候调用
     */
    public void initCreate() {
        // 窗口全屏
//        self.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // 竖屏
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    /**
     * 载入布局后,进行初始化
     * setContentView 后初始化配置
     */
    public void initBase() {
        //获取Application
        app = (AFishApplication) getApplication();
        //AFishActivity
        self = this;
        //上下文1
        context = this;
        //上下文2
        my = this;
        intent = self.getIntent();
        //控件注解绑定
        //ButterKnife.bind(this);
        //绑定按钮
        bindController();
        //初始化数据库存储
        //jwDb = FinalDb.create(self);
        //初始化网络
        jwHttp = new FinalHttp();
        //准备加载窗口 可升级
        pDialog = new ProgressDialog(self);
        pDialog.setMessage(getString(R.string.loading));
        //初始化otto事件总线(重要)
        OttoBus.getDefault().register(self);
        //当Activity 创建时,发送一个我打开了的消息,并把类名传输过去
        OttoBus.getDefault().post(getActCreateMsgEventClass());
        initLoading();
        configuration = (new Configuration.Builder()).setDuration(5000).build();

        //最后一个运行init
        initAct();
    }


    /**
     * 让子类来继承然后在setContentView后运行
     */
    public void initAct() {

    }

    /**
     * 判断导航栏是否隐藏
     *
     * @return
     */
    public boolean isHideNavcationBar() {
        return hideNavcationBar;
    }

    /**
     * 隐藏导航栏
     *
     * @param hideNavcationBar
     */
    public void setHideNavcationBar(boolean hideNavcationBar) {
        this.hideNavcationBar = hideNavcationBar;
    }

    /**
     * 后退按钮是否隐藏
     * @return
     */
    public boolean isHideBack() {
        return hideBack;
    }

    /**
     * 设置隐藏后退按钮
     * @param hideBack
     */
    public void setHideBack(boolean hideBack) {
        this.hideBack = hideBack;
    }

    /**
     * 获取导航栏颜色
     *
     * @return
     */
    public int getiNavcationBarColor() {
        return iNavcationBarColor;
    }

    /**
     * 单个设置导航栏颜色
     *
     * @param iNavcationBarColor
     */
    public void setiNavcationBarColor(int iNavcationBarColor) {
        this.iNavcationBarColor = iNavcationBarColor;
    }

    /**
     * 动态添加导航栏
     *
     * @param view
     */
    public View initNavcationBar(View view) {
        if (hideNavcationBar) {
            return view;
        } else {
            //先实例化基类布局
            LayoutInflater inflater = LayoutInflater.from(this);
            RelativeLayout root = null;
            if (PublicColors.getLayoutRID() != 0) {//动态设置导航栏
                root = (RelativeLayout) inflater.inflate(PublicColors.getLayoutRID(), null);
                navcationBar = (FrameLayout) root.findViewById(R.id.action_bar);
                title_tv = (TextView) root.findViewById(R.id.title_tv);
            } else {
                root = (RelativeLayout) inflater.inflate(R.layout.activity_actionbar, null);
                navcationBar = (FrameLayout) root.findViewById(R.id.action_bar);
                title_tv = (TextView) root.findViewById(R.id.title_tv);
                if (PublicColors.getNavicationBarColorRID() != 0) {
                    //设置全局导航栏
                    navcationBar.setBackgroundColor(getResources().getColor(PublicColors.getNavicationBarColorRID()));
                }
                if (PublicColors.getTextColorRID() != 0) {
                    //设置全局导航栏
                    title_tv.setTextColor(getResources().getColor(PublicColors.getTextColorRID()));
                }
                menu = (LinearLayout) root.findViewById(R.id.menu);
                //如果是Activity内部设置的时候,显示内部设置的颜色
                if (getiNavcationBarColor() != 0) {
                    //设置全局导航栏
                    navcationBar.setBackgroundColor(getResources().getColor(getiNavcationBarColor()));
                }
            }

            //实例化子类布局
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            //设置权重为1
//        params.weight = 1;
            params.addRule(RelativeLayout.BELOW, navcationBar.getId());
            //加入到基类布局中
            view.setLayoutParams(params);
            root.addView(view);
            return root;
        }
//        super.setContentView(root);
        //绑定按钮
    }


    /**
     * 显示跳动加载
     * 已淘汰
     */
    void initLoading() {
        loadingLayout = (RelativeLayout) findViewById(R.id.loadingLayout);
        reloadlayout = (RelativeLayout) findViewById(R.id.reloadlayout);
        if (OUtils.IsNotNull(reloadlayout)) {
            reloadlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OnLoadRefresh();
                }
            });
        }
    }

    /**
     * 初始化一个View的动画
     *
     * @param view
     */
    void initViewAnimation(View view) {
        mShowAction = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                -1.0f, Animation.RELATIVE_TO_SELF, 0.0f);
        mShowAction.setDuration(500);

        mHiddenAction = new TranslateAnimation(Animation.RELATIVE_TO_SELF,
                0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                -1.0f);
        mHiddenAction.setDuration(500);
    }

    /**
     * Activity生命周期结束
     */
    @Override
    protected void onDestroy() {
        //反注册事件总线
        // 不要忘记注销！！！！
        OttoBus.getDefault().unregister(self);
        super.onDestroy();
    }

    /**
     * 快速打开一个没有传递参数Activity
     *
     * @param cls
     */
    public void JwStartActivity(Class<?> cls) {
        Intent intent = new Intent(this, cls);
        //传递返回动画
        intent.putExtra("backAnimation", _animationList[animationPos]);
        self.startActivity(intent);
        ActAnimator();
    }

    /**
     * 快速打开一个有传递参数Activity
     *
     * @param cls
     */
    public void JwStartActivity(Class<?> cls, BaseItem baseItem) {
        Intent intent = new Intent(this, cls);
        intent.putExtra(StaticStrUtils.baseItem, baseItem);
        //传递返回动画
        intent.putExtra("backAnimation", _animationList[animationPos]);
        self.startActivity(intent);
        ActAnimator();
    }

    /**
     * 快速打开一个没有传递参数Activity
     *
     * @param cls
     */
    public void JwStartActivity(Class<?> cls, String params) {
        Intent intent = new Intent(this, cls);
        intent.putExtra(StaticStrUtils.baseItem, params);
        //传递返回动画
        intent.putExtra("backAnimation", _animationList[animationPos]);
        self.startActivity(intent);
        ActAnimator();
    }

    /**
     * 打开一个Intent,
     *
     * @param intent
     */
    public void JwStartActivity(Intent intent) {
        //传递返回动画
        intent.putExtra("backAnimation", _animationList[animationPos]);
        self.startActivity(intent);
        ActAnimator();
    }

    /**
     * 打开Activity并可以选择动画类型
     *
     * @param cls
     * @param iAnimationPos
     */
    public void JwStartActivity(Class<?> cls, int iAnimationPos) {
        Intent intent = new Intent(this, cls);
        //传递返回动画
        intent.putExtra("backAnimation", _animationList[iAnimationPos]);
        self.startActivity(intent);
        ActAnimator();
    }

    /**
     * 打开一个配置好的Intent 加上动画
     *
     * @param intent
     */
    public void JwConfigIntent(Intent intent) {
        //传递返回动画
        intent.putExtra("backAnimation", _animationList[animationPos]);
    }

    /**
     * 打开拥有WebView的Activity
     *
     * @param sUrl
     * @param sTitle 标题名称
     */
    public void JwStartWebActivity(String sUrl, String sTitle) {
        if (!StrUtils.isEmpty(sUrl) && !StrUtils.isEmpty(sTitle)) {
            Intent intent = new Intent(self,
                    UrlWebViewActivity.class);
            intent.putExtra(StaticStrUtils.htmlurl, sUrl);
            intent.putExtra(StaticStrUtils.title, sTitle);
            //传递返回动画
            intent.putExtra("backAnimation", _animationList[animationPos]);
            self.startActivity(intent);
            ActAnimator();
        }
    }

    /**
     * 打开拥有WebView的Activity
     *
     * @param sUrl
     */
    public void JwStartWebActivity(String sUrl) {
        if (!StrUtils.isEmpty(sUrl)) {
            Intent intent = new Intent(self,
                    UrlWebViewActivity.class);
            intent.putExtra(StaticStrUtils.htmlurl, sUrl);
            //传递返回动画
            intent.putExtra("backAnimation", _animationList[animationPos]);
            self.startActivity(intent);
            ActAnimator();
        }
    }

    /**
     * 加载窗口变量
     */
    private SFProgrssDialog m_customProgrssDialog;

    /**
     * 显示动画
     *
     * @param msg 加载文字
     */
    final public void showLoading(String msg) {
        if (null == m_customProgrssDialog)
            m_customProgrssDialog = SFProgrssDialog
                    .createProgrssDialog(this);
        if (null != m_customProgrssDialog) {
            m_customProgrssDialog.setMessage(msg);
            m_customProgrssDialog.show();
            m_customProgrssDialog.setCancelable(true);
        }
    }

    /**
     * 显示加载窗口
     */
    final public void showLoading() {
        showLoading(getString(R.string.Pleasewait));
    }


    /**
     * 隐藏加载窗口
     */
    final public void hideLoading() {
        if (null != m_customProgrssDialog) {
            m_customProgrssDialog.dismiss();
            m_customProgrssDialog = null;
        }
    }


    /**
     * Activity切换效果
     */
    public void ActAnimator() {
        try {
            ActivityAnimator anim = new ActivityAnimator();
//            动画效果
            anim.getClass().getMethod(_animationList[animationPos] + "Animation", Activity.class).invoke(anim, this);
//            anim.getClass().getMethod("PullRightPushLeft", Activity.class).invoke(anim, this);
        } catch (Exception e) {
//            Toast.makeText(this, "An error occured " + e.toString(), Toast.LENGTH_LONG).show();
        }
    }


    /**
     * 如果有返回按钮,那么绑定
     */
    private void bindController() {
        //防止错误,使框架稳定
        try {
            //如果有返回按钮,那么绑定
            btnBack = (Button) self.findViewById(R.id.btnBack);
            //如果按钮存在,那么强制绑定
            if (OUtils.IsNotNull(btnBack)) {
                btnBack.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        btnBackClick(v);
                    }
                });
                //设置返回按钮图标
                if (PublicColors.getBackBtnRID() != 0) {
                    if (OUtils.IsNotNull(btnBack)) {
                        btnBack.setBackgroundDrawable(ContextCompat.getDrawable(getMy(), PublicColors.getBackBtnRID()));
                    }
                }
                if (hideBack) {
                    btnBack.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            Log.e("JwActivity_bindBtn", e.getMessage());
        }
        //防止错误,使框架稳定
        try {
            //如果有返回按钮,那么绑定
            title_tv = (TextView) self.findViewById(R.id.title_tv);
            //如果按钮存在,那么强制绑定
            if (OUtils.IsNotNull(title_tv)) {
            }
        } catch (Exception e) {
            Log.e("JwActivity_bindBtn", e.getMessage());
        }
    }

    /**
     * 设置NavcationgBar Title
     *
     * @param sTitle
     */
    public void setTitle(String sTitle) {
        if (OUtils.IsNotNull(title_tv)) {
            title_tv.setText(StrUtils.IsNull(sTitle));
        }
    }

    /**
     * 后退按钮被单击
     * 绑定btnBack按钮
     *
     * @param view
     */
    public void btnBackClick(View view) {
        selfFinish();
    }

    /**
     * 返回按钮点击
     *
     * @param view
     */
    public void BackBtnClick(View view) {
        selfFinish();
    }

    /**
     * this.finish();
     * 以及一些动画的处理.
     * 默认动画
     */
    public void selfFinish() {
        this.finish();
        try {
            ActivityAnimator anim = new ActivityAnimator();
            anim.getClass().getMethod(this.getIntent().getExtras().getString("backAnimation") + "Animation", Activity.class).invoke(anim, this);
//            anim.getClass().getMethod("PullLeftPushRight", Activity.class).invoke(anim, this);

        } catch (Exception e) {
//            Toast.makeText(this, "An error occured " + e.toString(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * 程序返回关闭时
     */
    @Override
    public void onBackPressed() {
        selfFinish();
    }

    /**
     * 点击返回按键手动调用finish.这样既可使用默认动画
     *
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//            //点击返回按键手动调用finish.这样既可使用默认动画
//            selfFinish();
//            return true;
//        } else
        if (keyCode == KeyEvent.KEYCODE_MENU) {//MENU键
            //监控/拦截菜单键
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }


    /**
     * 点击空白处隐藏软键盘
     *
     * @param ev
     * @return
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {

            // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）
            View v = getCurrentFocus();

            if (isShouldHideInput(v, ev)) {
                hideSoftInput(v.getWindowToken());
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
     *
     * @param v
     * @param event
     * @return
     */
    private boolean isShouldHideInput(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
                    + v.getWidth();
            if (event.getX() > left && event.getX() < right
                    && event.getY() > top && event.getY() < bottom) {
                // 点击EditText的事件，忽略它。
                return false;
            } else {
                return true;
            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
        return false;
    }

    /**
     * 多种隐藏软件盘方法的其中一种
     *
     * @param token
     */
    private void hideSoftInput(IBinder token) {
        if (token != null) {
            InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(token,
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


    /**
     * 单次访问网络页面GET
     * 带参数
     *
     * @param sUrl
     * @param params
     */
    public void JwHttpGet(String sUrl, AjaxParams params) {
        JwHttpGet(sUrl, params, false);
    }

    /**
     * 网络访问,不带参数
     *
     * @param sUrl
     */
    public void JwHttpGet(String sUrl) {
        JwHttpGet(sUrl, null, false);
    }

    /**
     * 网络访问不带参数,显示正在加载框
     *
     * @param sUrl
     * @param isDialog
     */
    public void JwHttpGet(String sUrl, final boolean isDialog) {
        JwHttpGet(sUrl, null, isDialog);
    }

    /**
     * 带参数带动画的网络连接
     *
     * @param sUrl
     * @param params
     * @param isDialog
     */
    public void JwHttpGet(String sUrl, AjaxParams params, final boolean isDialog) {
        Logd(sUrl);
        jwHttp.get(sUrl, params, new AjaxCallBack<String>() {
            @Override
            public void onStart() {
                if (isDialog) {
                    showLoading();
                }
            }

            @Override
            public void onSuccess(String s) {
                ResMsgItem resMsgItem = AFishJSONCla.parseRes(s);
                if (OUtils.IsNotNull(resMsgItem)) {
                    HttpSuccess(resMsgItem);
                }
                HttpFinish();
                if (isDialog) {
                    hideLoading();
                }
            }

            @Override
            public void onFailure(Throwable t, int errorNo, String strMsg) {
                if (isDialog) {
                    hideLoading();
                }
                HttpFail(strMsg);
                HttpFinish();
            }
        });
    }


    /**
     * 带参数带动画的网络连接
     *
     * @param sUrl     URL
     * @param params   参数
     * @param isDialog 是否显示加载动画
     */
    public void JwHttpPost(String sUrl, AjaxParams params, final boolean isDialog) {
        Logd(sUrl);
        jwHttp.post(sUrl, params, new AjaxCallBack<String>() {
            @Override
            public void onStart() {
                if (isDialog) {
                    showLoading();
                }
            }

            @Override
            public void onSuccess(String s) {
                
                ResMsgItem resMsgItem = AFishJSONCla.parseRes(s);
                if (OUtils.IsNotNull(resMsgItem)) {
                    HttpSuccess(resMsgItem);
                }
                HttpFinish();
                if (isDialog) {
                    hideLoading();
                }
            }

            @Override
            public void onFailure(Throwable t, int errorNo, String strMsg) {
                if (isDialog) {
                    hideLoading();
                }
                HttpFail(strMsg);
                HttpFinish();
            }
        });
    }

    /**
     * 单次访问网络页面POST
     * 带参数
     *
     * @param sUrl
     * @param params
     */
    public void JwHttpPost(String sUrl, AjaxParams params) {
        JwHttpPost(sUrl, params, false);
    }

    /**
     * 最简单的POST
     * 基本没什么用
     *
     * @param sUrl
     */
    public void JwHttpPost(String sUrl) {
        JwHttpPost(sUrl, null, false);
    }

    /**
     * 捷微默认返回ResMsgItem模板
     *
     * @param resMsgItem
     */
    protected void HttpSuccess(ResMsgItem resMsgItem) {

    }


    /**
     * HTTP错误时
     *
     * @param strMsg
     */
    protected void HttpFail(String strMsg) {
        // ToastShowHttpFail();
        ToastShow(strMsg);
    }

    /**
     * HTTP完成时候
     */
    protected void HttpFinish() {

    }

    /**
     * 访问完成时
     */
    public void ToastShowHttpFail() {
        //网络错误时
        ToastShow(getString(R.string.isErrorIntent));
    }


    /**
     * 百度云推送事件绑定.
     * 无法直接使用注解@Subscribe声明
     * 继承回去再修改@Override成@Subscribe
     * 推送的时候可以传递到所有存活的集成与该类的Activity
     *
     * @param baiduPushEvent
     */
    public void onBaiduPushEvent(BaiduPushEvent baiduPushEvent) {
        //等子类继承处理
    }

    public void onBaiduPushEvent(PushExtraEvent pushExtraEvent) {
        //等子类继承处理
    }

    /**
     * 告诉一个Activity你该传值了
     * 无法直接使用注解@Subscribe声明
     * 继承回去再修改@Override成@Subscribe
     *
     * @param activityMsgEvent
     */
//    @Subscribe
    public void onActivityMsgEvent(ActivityMsgEvent activityMsgEvent) {
        //代替Intent传递实例化对象
    }

    /**
     * 快速生成一个该Activity类名为文件名的SharedPreferences
     */
    public void JwGetSharedPreferences() {
        sf = getSharedPreferences(this.getLocalClassName(),
                MODE_PRIVATE);
    }

    public ActivityMsgEvent getActCreateMsgEventClass() {
        return new ActivityMsgEvent(getActivityClass());
    }

    public String getActivityClass() {
        return this.getClass().getName();
    }

    //Loading
    public void loading() {
        if (OUtils.IsNotNull(loadingLayout) && OUtils.IsNotNull(reloadlayout)) {
            loadingLayout.setAnimation(mShowAction);
            loadingLayout.setVisibility(View.VISIBLE);
            reloadlayout.setAnimation(mHiddenAction);
            reloadlayout.setVisibility(View.GONE);
        }
    }

    public void loadCancle() {
        if (OUtils.IsNotNull(loadingLayout) && OUtils.IsNotNull(reloadlayout)) {
            loadingLayout.setAnimation(mHiddenAction);
            loadingLayout.setVisibility(View.GONE);
            reloadlayout.setAnimation(mHiddenAction);
            reloadlayout.setVisibility(View.GONE);
        }
    }

    public void loadFail() {
        if (OUtils.IsNotNull(loadingLayout) && OUtils.IsNotNull(reloadlayout)) {
            loadingLayout.setAnimation(mHiddenAction);
            loadingLayout.setVisibility(View.GONE);
            reloadlayout.setAnimation(mShowAction);
            reloadlayout.setVisibility(View.VISIBLE);
        }
    }

    public void OnLoadRefresh() {

    }

    //定位

    /**
     * 检测gps设置
     */
    public boolean checkGps() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled("gps")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("GPS未开启，开启GPS设置？").setCancelable(false)
                    .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
                        }
                    }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
            return false;
        }
        return true;
    }


    protected String getDeviceId() {
        TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceId = tm.getDeviceId();
        return deviceId;
    }


    // -----顶层显示Toast-----
    //
    public void AppMsgShowALERT(String msg) {
        AFishToast.AppMsgShowALERT(self, msg);
    }

    public void AppMsgShowCONFIRM(String msg) {
        AFishToast.AppMsgShowCONFIRM(self, msg);
    }

    public void AppMsgShowINFO(String msg) {
        AFishToast.AppMsgShowINFO(self, msg);
    }

    public void CroutonShowALERT(String msg) {
        AFishToast.CroutonShowALERT(self, msg);
    }

    public void CroutonShowCONFIRM(String msg) {
        AFishToast.CroutonShowCONFIRM(self, msg);
    }

    public void CroutonShowINFO(String msg) {
        AFishToast.CroutonShowINFO(self, msg);
    }


    // -----标题栏下显示Toast-----
    //如果setContentView Layout 没有CroutonLayout布局自动显示在标题栏上
    //红色
    public void CroutonALERT(String msg) {
        if (StrUtils.isEmpty(msg)) {
            return;
        }
        ViewGroup viewGroup = (ViewGroup) self.findViewById(R.id.CroutonLayout);
        if (OUtils.IsNotNull(viewGroup)) {
            Crouton.makeText(self, msg, Style.ALERT, viewGroup).
                    setConfiguration(configuration).show();
        } else {
            CroutonShowALERT(msg);
        }
    }

    //黄色
    public void CroutonCONFIRM(String msg) {
        if (StrUtils.isEmpty(msg)) {
            return;
        }
        ViewGroup viewGroup = (ViewGroup) self.findViewById(R.id.CroutonLayout);
        if (OUtils.IsNotNull(viewGroup)) {
            Crouton.makeText(self, msg, Style.CONFIRM, viewGroup).
                    setConfiguration(configuration).show();
        } else {
            CroutonShowCONFIRM(msg);
        }
    }

    //蓝色
    public void CroutonINFO(String msg) {
        if (StrUtils.isEmpty(msg)) {
            return;
        }
        ViewGroup viewGroup = (ViewGroup) self.findViewById(R.id.CroutonLayout);
        if (OUtils.IsNotNull(viewGroup)) {
            Crouton.makeText(self, msg, Style.INFO, viewGroup).
                    setConfiguration(configuration).show();
        } else {
            CroutonShowINFO(msg);
        }
    }

    public int getAnimationPos() {
        return animationPos;
    }

    public void setAnimationPos(int animationPos) {
        this.animationPos = animationPos;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Context getMy() {
        return my;
    }

    public void setMy(Context my) {
        this.my = my;
    }

//    public Resources getResource() {
//        return resource;
//    }
//
//    public void setResource(Resources resource) {
//        this.resource = resource;
//    }

//    public String getPkgName() {
//        return pkgName;
//    }
//
//    public void setPkgName(String pkgName) {
//        this.pkgName = pkgName;
//    }

    // -----标题栏下显示Toast-----
    //如果setContentView Layout 没有CroutonLayout布局自动显示在标题栏上
    //----------------------------

    /**
     * 快速显示Toast
     *
     * @param msg
     */
    public void ToastShow(String msg) {
        AFishToast.ToastShow(msg);
    }

    //快捷Log-----------------------
    public void Logv(String message) {
        AFishLog.v(message);
    }

    public void Logd(String message) {
        AFishLog.d(message);
    }

    public void Logi(String message) {
        AFishLog.i(message);
    }

    public void Logw(String message) {
        AFishLog.w(message);
    }

    public void Loge(String message) {
        AFishLog.e(message);
    }


    /**
     * 文字菜单
     *
     * @author lyixin@linewell.com
     * @since 2014年11月25日
     */
    public class MenuTextView extends Button {

        public MenuTextView(Context context) {
            super(context);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(android.widget.LinearLayout.LayoutParams.WRAP_CONTENT, android.widget.LinearLayout.LayoutParams.MATCH_PARENT);
            setLayoutParams(params);
            setPadding(6, 6, 3, 6);
            setTextSize(14);
            setTextColor(getResources().getColor(R.color.black));
            setBackgroundResource(R.drawable.list_selector_holo_light);
        }
    }

    /**
     * 图片菜单
     *
     * @author lyixin@linewell.com
     * @since 2014年11月25日
     */
    public class MenuImageView extends ImageButton {

        public MenuImageView(Context context) {
            super(context);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(NumberHelper.dip2px(getApplicationContext(), 50), android.widget.LinearLayout.LayoutParams.MATCH_PARENT);
            setLayoutParams(params);
            setPadding(6, 6, 6, 6);
            setAdjustViewBounds(true);
            setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            setBackgroundResource(R.drawable.list_selector_holo_light);
        }
    }

    /**
     * 设置右上角的菜单项
     * 2014年11月12日
     *
     * @param view
     */
    public void addMenuView(View view) {
        menu.addView(view);
    }

    public void removeMenuView(View view) {
        menu.removeView(view);
    }

    //添加导航栏背景图片
    public void setNavcationBarBackgroud(int rid) {
        navcationBar.setBackgroundResource(rid);
    }
}
