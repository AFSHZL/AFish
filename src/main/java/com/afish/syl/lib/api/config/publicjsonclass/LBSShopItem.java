package com.afish.syl.lib.api.config.publicjsonclass;

/**
 * Created by Administrator on 2014-12-25.
 */
public class LBSShopItem {
    private String sn;
    private String name;
    private String shop_type;
    private String p_id;
    private String tzsy;
    private String from_t;
    private String province;
    private String city;
    private String district;
    private String shop_address;
    private String shop_lxr;
    private String lbsaddress;
    private Double lbsjd;
    private Double lbswd;

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShop_type() {
        return shop_type;
    }

    public void setShop_type(String shop_type) {
        this.shop_type = shop_type;
    }

    public String getP_id() {
        return p_id;
    }

    public void setP_id(String p_id) {
        this.p_id = p_id;
    }

    public String getTzsy() {
        return tzsy;
    }

    public void setTzsy(String tzsy) {
        this.tzsy = tzsy;
    }

    public String getFrom_t() {
        return from_t;
    }

    public void setFrom_t(String from_t) {
        this.from_t = from_t;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getShop_address() {
        return shop_address;
    }

    public void setShop_address(String shop_address) {
        this.shop_address = shop_address;
    }

    public String getShop_lxr() {
        return shop_lxr;
    }

    public void setShop_lxr(String shop_lxr) {
        this.shop_lxr = shop_lxr;
    }

    public String getLbsaddress() {
        return lbsaddress;
    }

    public void setLbsaddress(String lbsaddress) {
        this.lbsaddress = lbsaddress;
    }

    public Double getLbsjd() {
        return lbsjd;
    }

    public void setLbsjd(Double lbsjd) {
        this.lbsjd = lbsjd;
    }

    public Double getLbswd() {
        return lbswd;
    }

    public void setLbswd(Double lbswd) {
        this.lbswd = lbswd;
    }
}
