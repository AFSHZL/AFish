package com.afish.syl.lib.api.config.publicjsonclass;

/**
 * Created by Administrator on 2015-03-10.
 */
public class MenberInfoItem {
    private String id;
    private String menber_sn;
    private String menber_name;
    private String nick_name;
    private String img_url;
    private String kefu;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMenber_sn() {
        return menber_sn;
    }

    public void setMenber_sn(String menber_sn) {
        this.menber_sn = menber_sn;
    }

    public String getMenber_name() {
        return menber_name;
    }

    public void setMenber_name(String menber_name) {
        this.menber_name = menber_name;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getKefu() {
        return kefu;
    }

    public void setKefu(String kefu) {
        this.kefu = kefu;
    }
}
