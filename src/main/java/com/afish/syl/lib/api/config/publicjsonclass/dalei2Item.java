package com.afish.syl.lib.api.config.publicjsonclass;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2014-12-24.
 */
public class dalei2Item {
    private String id;
    private String dl_sn;
    private String dl_name;
    private String note;
    private String img_1;
    private List<fjsx1Item> fjsx1 = new ArrayList<fjsx1Item>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDl_sn() {
        return dl_sn;
    }

    public void setDl_sn(String dl_sn) {
        this.dl_sn = dl_sn;
    }

    public String getDl_name() {
        return dl_name;
    }

    public void setDl_name(String dl_name) {
        this.dl_name = dl_name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getImg_1() {
        return img_1;
    }

    public void setImg_1(String img_1) {
        this.img_1 = img_1;
    }

    public List<fjsx1Item> getFjsx1() {
        return fjsx1;
    }

    public void setFjsx1(List<fjsx1Item> fjsx1) {
        this.fjsx1 = fjsx1;
    }
}
