package com.afish.syl.lib.api.component.controller;

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageView;

/**
 * 按钮只要默认放一张底图,继承该类的按钮被按下的时候底图会自动变暗
 * SylButton
 * Created by 苏逸龙 on 2015-08-11.
 */
public class AFishButton extends Button implements OnTouchListener, OnFocusChangeListener {

    public final float[] BT_SELECTED = new float[]{1, 0, 0, 0, -25, 0, 1,
            0, 0, -25, 0, 0, 1, 0, -25, 0, 0, 0, 1, 0};
    public final float[] BT_NOT_SELECTED = new float[]{1, 0, 0, 0, 0, 0,
            1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0};

    public AFishButton(Context context) {
        super(context);
        this.setOnTouchListener(this);
        this.setOnFocusChangeListener(this);
    }

    public AFishButton(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.buttonStyle);
        this.setOnTouchListener(this);
        this.setOnFocusChangeListener(this);
    }

    public AFishButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFocusable(true);
        this.setOnTouchListener(this);
        this.setOnFocusChangeListener(this);
    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        if (hasFocus) {
            if (v instanceof ImageView) {
                ImageView iv = (ImageView) v;
                iv.setColorFilter(new ColorMatrixColorFilter(BT_SELECTED));
            } else {
                v.getBackground().setColorFilter(new ColorMatrixColorFilter(BT_SELECTED));
                v.setBackgroundDrawable(v.getBackground());
            }
        } else {
            if (v instanceof ImageView) {
                ImageView iv = (ImageView) v;
                iv.setColorFilter(new ColorMatrixColorFilter(BT_NOT_SELECTED));
            } else {
                v.getBackground().setColorFilter(
                        new ColorMatrixColorFilter(BT_NOT_SELECTED));
                v.setBackgroundDrawable(v.getBackground());
            }
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (v instanceof ImageView) {
                ImageView iv = (ImageView) v;
                iv.setColorFilter(new ColorMatrixColorFilter(BT_SELECTED));
            } else {
                v.getBackground().setColorFilter(new ColorMatrixColorFilter(BT_SELECTED));
                v.setBackgroundDrawable(v.getBackground());
            }
        } else if ((event.getAction() == MotionEvent.ACTION_UP) || (event.getAction() == MotionEvent.ACTION_CANCEL)) {
            if (v instanceof ImageView) {
                ImageView iv = (ImageView) v;
                iv.setColorFilter(new ColorMatrixColorFilter(BT_NOT_SELECTED));
            } else {
                v.getBackground().setColorFilter(
                        new ColorMatrixColorFilter(BT_NOT_SELECTED));
                v.setBackgroundDrawable(v.getBackground());
            }
        }
        return false;
    }

}
