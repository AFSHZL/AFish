package com.afish.syl.lib.api.config.publicjsonclass;

/**
 * Created by Administrator on 2015-08-18.
 */
public class LoadBuyItem {
    String flag;
    String memberid;
    String productid;
    double buyintegral;
    String buyid;
    String buydate;
    String membername;
    String productname;
    String remark;
    LoadProductItem productModel;

    public LoadProductItem getProductModel() {
        return productModel;
    }

    public void setProductModel(LoadProductItem productModel) {
        this.productModel = productModel;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMemberid() {
        return memberid;
    }

    public void setMemberid(String memberid) {
        this.memberid = memberid;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public double getBuyintegral() {
        return buyintegral;
    }

    public void setBuyintegral(double buyintegral) {
        this.buyintegral = buyintegral;
    }

    public String getBuyid() {
        return buyid;
    }

    public void setBuyid(String buyid) {
        this.buyid = buyid;
    }

    public String getBuydate() {
        return buydate;
    }

    public void setBuydate(String buydate) {
        this.buydate = buydate;
    }

    public String getMembername() {
        return membername;
    }

    public void setMembername(String membername) {
        this.membername = membername;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
