package com.afish.syl.lib.api.core.afishpublic.common.util;

import com.afish.syl.lib.api.core.afishpublic.common.entity.PatchResult;

/**
 * PatchUtils
 * 
 * @author  2013-12-10
 */
public class PatchUtils {

    /**
     * patch old apk and patch file to new apk
     * 
     * @param oldApkPath
     * @param patchPath
     * @param newApkPath
     * @return
     */
    public static native PatchResult patch(String oldApkPath, String patchPath, String newApkPath);
}
