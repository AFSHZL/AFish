package com.afish.syl.lib.api.core.otto;


import com.afish.syl.lib.api.afishlib.baidumaps.LocationInfo;

/**
 * 需要定位信息时发送该消息
 * 作者：苏逸龙 on 2015-09-01 20:39
 * QQ：317616660
 */
public class LocInfoEvent {
    String msg;
    String json;
    LocationInfo locationInfo;

    public LocInfoEvent(LocationInfo locationInfo) {
        this.locationInfo = locationInfo;
    }

    public LocInfoEvent(String msg, LocationInfo locationInfo) {
        this.msg = msg;
        this.locationInfo = locationInfo;
    }

    public LocationInfo getLocationInfo() {
        return locationInfo;
    }

    public void setLocationInfo(LocationInfo locationInfo) {
        this.locationInfo = locationInfo;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }
}
