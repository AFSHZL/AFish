package com.afish.syl.lib.api.core.afishpublic.common.constant;

/**
 * HttpConstants
 * <strong>所有小写</strong>
 * 
 * @author  2013-5-12
 */
public class HttpConstants {

    public static final String EXPIRES       = "expires";
    public static final String CACHE_CONTROL = "cache-control";
}
