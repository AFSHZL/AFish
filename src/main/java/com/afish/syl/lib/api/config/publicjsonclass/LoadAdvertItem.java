package com.afish.syl.lib.api.config.publicjsonclass;

/**
 * Created by Administrator on 2015-08-13.
 */
public class LoadAdvertItem {
    String name;
    String flag;
    String categoryid;
    String halfPic;
    int halfPic_width;
    int halfPic_height;
    int picpath_width;
    int picpath_height;
    String picpath;
    String title;
    String url;
    int integral;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(String categoryid) {
        this.categoryid = categoryid;
    }

    public String getHalfPic() {
        return halfPic;
    }

    public void setHalfPic(String halfPic) {
        this.halfPic = halfPic;
    }

    public int getHalfPic_width() {
        return halfPic_width;
    }

    public void setHalfPic_width(int halfPic_width) {
        this.halfPic_width = halfPic_width;
    }

    public int getHalfPic_height() {
        return halfPic_height;
    }

    public void setHalfPic_height(int halfPic_height) {
        this.halfPic_height = halfPic_height;
    }

    public int getPicpath_width() {
        return picpath_width;
    }

    public void setPicpath_width(int picpath_width) {
        this.picpath_width = picpath_width;
    }

    public int getPicpath_height() {
        return picpath_height;
    }

    public void setPicpath_height(int picpath_height) {
        this.picpath_height = picpath_height;
    }

    public String getPicpath() {
        return picpath;
    }

    public void setPicpath(String picpath) {
        this.picpath = picpath;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getIntegral() {
        return integral;
    }

    public void setIntegral(int integral) {
        this.integral = integral;
    }
}
