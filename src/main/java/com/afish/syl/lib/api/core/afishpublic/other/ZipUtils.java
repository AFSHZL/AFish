package com.afish.syl.lib.api.core.afishpublic.other;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * @author 苏逸龙     317616660@qq.com
 * @ClassName: ZipUtils
 * @Description: TODO(字符串压缩类)
 * @date 2014-3-15 上午10:59:34
 */
public class ZipUtils {

    /**
     * MethodName:compress
     * Method description
     * 压缩
     *
     * @param str
     *
     * @return
     *
     * @throws IOException
     */
    public static String compress(String str) throws IOException {
        if ((str == null) || (str.length() == 0)) {
            return str;
        }

        ByteArrayOutputStream out  = new ByteArrayOutputStream();
        GZIPOutputStream      gzip = new GZIPOutputStream(out);

        gzip.write(str.getBytes());
        gzip.close();

        return out.toString("ISO-8859-1");
    }


    /**
     * MethodName:uncompress
     * Method description
     * 解压缩
     *
     * @param str
     *
     * @return
     *
     * @throws IOException
     */
    public static String uncompress(String str) throws IOException {
        if ((str == null) || (str.length() == 0)) {
            return str;
        }

        ByteArrayOutputStream out    = new ByteArrayOutputStream();
        ByteArrayInputStream  in     = new ByteArrayInputStream(str.getBytes("ISO-8859-1"));
        GZIPInputStream       gunzip = new GZIPInputStream(in);
        byte[]                buffer = new byte[256];
        int                   n;

        while ((n = gunzip.read(buffer)) >= 0) {
            out.write(buffer, 0, n);
        }

        // toString()使用平台默认编码，也可以显式的指定如toString("GBK")
        return out.toString();
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
