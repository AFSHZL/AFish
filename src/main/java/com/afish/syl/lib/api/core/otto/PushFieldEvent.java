package com.afish.syl.lib.api.core.otto;

/**
 * 广播Push 自定义字段消息
 * 作者：苏逸龙 on 2015-09-01 20:39
 * QQ：317616660
 */
public class PushFieldEvent {
    String key;
    String value;

    public PushFieldEvent() {
    }

    public PushFieldEvent(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
