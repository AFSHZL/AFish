package com.afish.syl.lib.api.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.text.TextUtils;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.TextView;

import com.afish.syl.lib.R;

/**
 * ClassName: SFProgrssDialog
 * Class description
 * 正在加载的Dialog
 *
 * @version        1.0, 16/01/27
 * @author         syl    
 */
public class SFProgrssDialog extends Dialog {

    /** Field 静态的Dialog */
    private static SFProgrssDialog m_progrssDialog;

    private SFProgrssDialog(Context context, int theme) {
        super(context, theme);
    }

    /**
     * MethodName:createProgrssDialog
     * Method description
     * 创建Dialog
     *
     * @param context
     *
     * @return
     */
    public static SFProgrssDialog createProgrssDialog(Context context) {
        m_progrssDialog = new SFProgrssDialog(context, R.style.SF_pressDialogCustom);
        m_progrssDialog.setContentView(R.layout.sf_view_custom_progress_dialog);
        m_progrssDialog.getWindow().getAttributes().gravity = Gravity.CENTER;

        return m_progrssDialog;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (null == m_progrssDialog) {
            return;
        }

        ImageView         loadingImageView  =
            (ImageView) m_progrssDialog.findViewById(R.id.sf_iv_progress_dialog_loading);
        AnimationDrawable animationDrawable = (AnimationDrawable) loadingImageView.getBackground();

        animationDrawable.start();
    }

    /**
     * MethodName:setMessage
     * Method description
     * 设置加载圈后面的文字
     *
     * @param msg
     *
     * @return
     */
    public SFProgrssDialog setMessage(String msg) {
        TextView loadingTextView = (TextView) m_progrssDialog.findViewById(R.id.sf_tv_progress_dialog_loading);

        if (!TextUtils.isEmpty(msg)) {
            loadingTextView.setText(msg);
        }

//      }
//      loadingTextView.setText(R.string.sf_progress_dialog_image_loading);
        return m_progrssDialog;
    }
}


//~ Formatted by syl
