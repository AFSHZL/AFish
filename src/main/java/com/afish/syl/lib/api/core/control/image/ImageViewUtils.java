package com.afish.syl.lib.api.core.control.image;

import android.view.ViewGroup.LayoutParams;

import android.widget.ImageView;

/**
 * @author 苏逸龙     317616660@qq.com
 * @ClassName: ImageViewUtils
 * @Description: TODO(设置ImageView的各种参数)
 * @date 2014-3-10 下午7:04:32
 */
public class ImageViewUtils {

    /**
     * @Title: setImageWidth
     * @Description: TODO(设置ImageView的宽)
     */
    public static void setImageWidth(ImageView imageView, Integer imageWidthInteger) {
        LayoutParams imgParams;

        imgParams        = imageView.getLayoutParams();
        imgParams.height = imageWidthInteger;
        imageView.setLayoutParams(imgParams);
    }
}


//~ Formatted by syl
