package com.afish.syl.lib.api.core.afishutil;


/**
 * 通用常量类
 * @author zzhan@linewell.com
 * @since 2014-3-18
 */
public class Common {

	/**
	 * 用SharedPreferences保存的文件名
	 */
	public static final String SHARED_PREFERENCES_FILE_NAME = "qzgrid_share";
	
	/**
	 * 用SharedPreferences保存的app信息上传标志
	 */
	public static final String SHARED_PREFERENCES_APPINFO_UPLOAD = "qzgrid_app_upload";
	
	/**
	 * 服务端传回的json字符串中的结果字段
	 */
	public static final String RESUTL = "success";
	/**
	 * 服务端传回的json字符串中的消息字段
	 */
	public static final String MESSAGE = "msg";

	/**
	 * 服务端传回的json字符串中的内容字段
	 */
	public static final String CONTENT = "obj";
	
}
