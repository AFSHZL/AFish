package com.afish.syl.lib.api.core.afishpublic.string;

/**
 * 字符串加密解密类
 * 作者：苏逸龙 on 2015-08-28 10:51
 * QQ：317616660
 */
public class StrCryptUtils {

    /**
     * //MD5加密一次后再加上一个key值再次加密 MD5CryptPassword
     *
     * @param sEncode
     * @param sKey
     * @return
     */
    public static String MD5CryptPwd(String sEncode, String sKey) {
        String md5passString = new String("");

        md5passString = StrUtils.MD5_32(sEncode);

        return md5passString = StrUtils.MD5_32(md5passString + sKey);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
