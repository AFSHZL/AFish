package com.afish.syl.lib.api.config.publicjsonclass;

/**
 * Created by Administrator on 2015-03-08.
 */
public class PinglunItem {
    private String menber_sn;
    private String business_sn;
    private String note;
    private String fwpj;
    private String shpj;
    private String dcsdpj;
    private String add_time;
    private String nick_name;


    public String getMenber_sn() {
        return menber_sn;
    }

    public void setMenber_sn(String menber_sn) {
        this.menber_sn = menber_sn;
    }

    public String getBusiness_sn() {
        return business_sn;
    }

    public void setBusiness_sn(String business_sn) {
        this.business_sn = business_sn;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getFwpj() {
        return fwpj;
    }

    public void setFwpj(String fwpj) {
        this.fwpj = fwpj;
    }

    public String getShpj() {
        return shpj;
    }

    public void setShpj(String shpj) {
        this.shpj = shpj;
    }

    public String getDcsdpj() {
        return dcsdpj;
    }

    public void setDcsdpj(String dcsdpj) {
        this.dcsdpj = dcsdpj;
    }

    public String getAdd_time() {
        return add_time;
    }

    public void setAdd_time(String add_time) {
        this.add_time = add_time;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }
}
