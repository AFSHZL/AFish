package com.afish.syl.lib.api.config.publicjsonclass;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2014-12-31.
 */
public class FilterItem {
    String sn;
    String name;
    Integer shop_type;
    Integer p_id;
    Integer tzsy;
    Integer from_t;
    private List<LBSShopItem> shop = new ArrayList<LBSShopItem>();
    private List<CangkuItem> cangku = new ArrayList<CangkuItem>();

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getShop_type() {
        return shop_type;
    }

    public void setShop_type(Integer shop_type) {
        this.shop_type = shop_type;
    }

    public Integer getP_id() {
        return p_id;
    }

    public void setP_id(Integer p_id) {
        this.p_id = p_id;
    }

    public Integer getTzsy() {
        return tzsy;
    }

    public void setTzsy(Integer tzsy) {
        this.tzsy = tzsy;
    }

    public Integer getFrom_t() {
        return from_t;
    }

    public void setFrom_t(Integer from_t) {
        this.from_t = from_t;
    }

    public List<LBSShopItem> getShop() {
        return shop;
    }

    public void setShop(List<LBSShopItem> shop) {
        this.shop = shop;
    }

    public List<CangkuItem> getCangku() {
        return cangku;
    }

    public void setCangku(List<CangkuItem> cangku) {
        this.cangku = cangku;
    }
}
