package com.afish.syl.lib.api.core.afishpublic.date;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;

/**
 * 日期处理类 AFishDateUtils
 * 作者：苏逸龙 on 2015-01-08
 * QQ：317616660
 */
public class AFishDateUtils {

    /** Field serialVersionUID */
    private static final long serialVersionUID = 3193922028309094171L;


    /**
     * MethodName:ConverToDate
     * Method description
     * 把字符串转为日期
     *
     * @param strDate
     *
     * @return
     *
     * @throws Exception
     */
    public static Date ConverToDate(String strDate) throws Exception {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        return df.parse(strDate);
    }


    /**
     * MethodName:ConverToDate2
     * Method description
     * 把字符串转为日期
     *
     * @param strDate
     *
     * @return
     *
     * @throws Exception
     */
    public static Date ConverToDate2(String strDate) throws Exception {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        return df.parse(strDate);
    }


    /**
     * MethodName:ConverToString
     * Method description
     * 把日期转为字符串
     *
     * @param date
     *
     * @return
     */
    public static String ConverToString(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        return df.format(date);
    }

    /**
     * MethodName:DateStrToChinaDateStr
     * Method description
     * 日期字符串,格式化成中文字符串
     *
     * @param strDate
     *
     * @return
     *
     * @throws Exception
     */
    public static String DateStrToChinaDateStr(String strDate) throws Exception {
        Date       date = ConverToDate(strDate);
        DateFormat df   = new SimpleDateFormat("yyyy年-MM月-dd日");

        return df.format(date);
    }

    /**
     * MethodName:DateStrToChinaStr
     * Method description
     * 日期字符串转中国日期
     *
     * @param strDate
     *
     * @return
     */
    public static String DateStrToChinaStr(String strDate) {
        String     res  = "";
        Date       date = null;
        DateFormat df   = null;

        try {
            date = ConverToDate(strDate);
            df   = new SimpleDateFormat("yyyy年MM月dd日");
            res  = df.format(date);
        } catch (Exception e) {}

        return res;
    }


    /**
     * MethodName:DateStrToDateTimeDesc
     * Method description
     * 把日期转为字符串 区分上午下午
     *
     * @param sDate
     *
     * @return
     */
    public static String DateStrToDateTimeDesc(String sDate) {
        Date date = null;

        try {
            date = ConverToDate2(sDate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        sDate = sDate.substring(11, 13);

        int        ih = Integer.valueOf(sDate);
        DateFormat df = null;

        if (ih > 12) {
            df = new SimpleDateFormat("yyyy-MM-dd 下午 hh:mm");
        } else {
            df = new SimpleDateFormat("yyyy-MM-dd 下午 hh:mm");
        }

        return df.format(date);
    }

    /**
     * MethodName:nDaysAfter
     * Method description
     * 后几天
     *
     * @param n
     * @param date
     *
     * @return
     */
    public static Date nDaysAfter(int n, Date date) {
        Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) + n);

        return cal.getTime();
    }

    /**
     * MethodName:nDaysAgo
     * Method description
     * 前几天
     *
     * @param n
     * @param date
     *
     * @return
     */
    public static Date nDaysAgo(Integer n, Date date) {
        Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) - n);

        return cal.getTime();
    }

    /**
     * MethodName:nMonthsAgo
     * Method description
     * 上个月
     *
     * @param n
     * @param date
     *
     * @return
     */
    public static Date nMonthsAgo(Integer n, Date date) {
        Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) - n);

        return cal.getTime();
    }

    /**
     * 获取增加多少天的时间
     *
     *
     * @param addDay
     * @return addDay - 增加多少天
     */
    public static Date getAddDayDate(int addDay) {
        Calendar calendar = Calendar.getInstance();

        calendar.add(Calendar.DAY_OF_YEAR, addDay);

        return calendar.getTime();
    }

    /**
     * 获取增加多少小时的时间
     *
     *
     * @param addHour
     * @return addDay - 增加多少消失
     */
    public static Date getAddHourDate(int addHour) {
        Calendar calendar = Calendar.getInstance();

        calendar.add(Calendar.HOUR, addHour);

        return calendar.getTime();
    }

    /**
     * 获取增加多少月的时间
     *
     *
     * @param addMonth
     * @return addMonth - 增加多少月
     */
    public static Date getAddMonthDate(int addMonth) {
        Calendar calendar = Calendar.getInstance();

        calendar.add(Calendar.MONTH, addMonth);

        return calendar.getTime();
    }

    /**
     * MethodName:getDayBegin
     * Method description
     * 开始的第一天
     *
     * @param date
     *
     * @return
     */
    public static Date getDayBegin(Date date) {
        Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);

        return cal.getTime();
    }

    /**
     * MethodName:getDayEnd
     * Method description
     * 结束的那一天
     *
     * @param date
     *
     * @return
     */
    public static Date getDayEnd(Date date) {
        Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH) + 1, 0, 0, 0);
        cal.set(Calendar.SECOND, cal.get(Calendar.SECOND) - 1);

        return cal.getTime();
    }

    /**
     * MethodName:getMonthBegin
     * Method description
     *
     *
     * @param date
     *
     * @return
     */
    public static Date getMonthBegin(Date date) {
        Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), 1, 0, 0, 0);

        return cal.getTime();
    }

    /**
     * MethodName:getMonthEnd
     * Method description
     *
     *
     * @param date
     *
     * @return
     */
    public static Date getMonthEnd(Date date) {
        Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, 1, 0, 0, 0);
        cal.set(Calendar.SECOND, cal.get(Calendar.SECOND) - 1);

        return cal.getTime();
    }

    /**
     * MethodName:getMonthOfThisYear
     * Method description
     *
     *
     * @return
     */
    public static Integer getMonthOfThisYear() {
        Calendar cal = Calendar.getInstance();

        cal.setTime(new Date());

        return cal.get(Calendar.MONTH) + 1;
    }

    /**
     * MethodName:getTimeByMonth
     * Method description
     *
     *
     * @param month
     *
     * @return
     */
    public static Date getTimeByMonth(Integer month) {
        Calendar cal = Calendar.getInstance();

        cal.setTime(new Date());
        cal.set(Calendar.MONTH, month - 1);

        return cal.getTime();
    }

    /**
     * MethodName:getWeekBegin
     * Method description
     *
     *
     * @param date
     *
     * @return
     */
    public static Date getWeekBegin(Date date) {
        Calendar cal = Calendar.getInstance();

        cal.setTime(date);

        Date mm = nDaysAgo(cal.get(Calendar.DAY_OF_WEEK) - 2, date);

        return getDayBegin(mm);
    }

    /**
     * MethodName:getWeekEnd
     * Method description
     *
     *
     * @param date
     *
     * @return
     */
    public static Date getWeekEnd(Date date) {
        Calendar cal = Calendar.getInstance();

        cal.setTime(date);

        Date mm = nDaysAfter(8 - cal.get(Calendar.DAY_OF_WEEK), date);

        return getDayEnd(mm);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
