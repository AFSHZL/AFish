package com.afish.syl.lib.api.component.controller;

import android.content.Context;

import android.util.AttributeSet;

import android.widget.GridView;

/**
 * ClassName: MyGridView
 * Class description
 * ScrollView和GridView 结合的时候
 * 会造成GridView显示不全
 * 使用该自定义控件解决该问题
 *
 * @version        1.0, 16/01/27
 * @author         syl    
 */
public class MyGridView extends GridView {

    /**
     * Constructs ...
     *
     *
     * @param context
     */
    public MyGridView(Context context) {
        super(context);
    }

    /**
     * Constructs ...
     *
     *
     * @param context
     * @param attrs
     */
    public MyGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Constructs ...
     *
     *
     * @param context
     * @param attrs
     * @param defStyle
     */
    public MyGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);

        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
