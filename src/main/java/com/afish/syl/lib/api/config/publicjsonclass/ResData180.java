package com.afish.syl.lib.api.config.publicjsonclass;

/**
 * Created by Administrator on 2015-08-06.
 */
public class ResData180 {
    private Boolean R;
    private String I;
    private String D;

    public Boolean getR() {
        return R;
    }

    public void setR(Boolean r) {
        R = r;
    }

    public String getI() {
        return I;
    }

    public void setI(String i) {
        I = i;
    }

    public String getD() {
        return D;
    }

    public void setD(String d) {
        D = d;
    }
}
