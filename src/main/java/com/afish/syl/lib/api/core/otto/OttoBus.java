package com.afish.syl.lib.api.core.otto;

import com.squareup.otto.Bus;

/**
 * 优雅的时间总线
 * 控制所有传递的事件
 * 消息核心终端
 * 可直接传递实例化的类
 * 无需序列化传输
 * 作者：苏逸龙 on 2015-09-01 16:38
 * QQ：317616660
 */
public class OttoBus {

    /** Field 为了方便设置成静态的Otto */
    private static final Bus BUS = new Bus();

    /**
     * MethodName:getDefault
     * Method description
     * 获取默认的事件总线
     * 定义了一个默认的,时间总线可以有多个
     *
     * @return
     */
    public static Bus getDefault() {
        return BUS;
    }
}


//~ Formatted by syl