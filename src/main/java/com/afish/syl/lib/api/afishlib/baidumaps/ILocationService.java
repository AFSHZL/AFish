package com.afish.syl.lib.api.afishlib.baidumaps;

public interface ILocationService {

	void locate();
	void locateStop();
}
