package com.afish.syl.lib.api.config.publicjsonclass;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015-01-03.
 */
public class testItem {
    private String dldm;
    private int id;
    private String business_sn;
    private String business_name;
    private String reg_time;
    private String reg_ly;
    private String qq;
    private String tel;
    private String moblie_phone;
    private List<PinglunItem> img_list = new ArrayList<PinglunItem>();

    public String getDldm() {
        return dldm;
    }

    public void setDldm(String dldm) {
        this.dldm = dldm;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBusiness_sn() {
        return business_sn;
    }

    public void setBusiness_sn(String business_sn) {
        this.business_sn = business_sn;
    }

    public String getBusiness_name() {
        return business_name;
    }

    public void setBusiness_name(String business_name) {
        this.business_name = business_name;
    }

    public String getReg_time() {
        return reg_time;
    }

    public void setReg_time(String reg_time) {
        this.reg_time = reg_time;
    }

    public String getReg_ly() {
        return reg_ly;
    }

    public void setReg_ly(String reg_ly) {
        this.reg_ly = reg_ly;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getMoblie_phone() {
        return moblie_phone;
    }

    public void setMoblie_phone(String moblie_phone) {
        this.moblie_phone = moblie_phone;
    }
}
