package com.afish.syl.lib.api.core.base;


import android.app.Application;
import android.content.Context;

import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;




/**
 * @author 苏逸龙     317616660@qq.com
 * @ClassName: AFishApplication
 * @Description: TODO(Baidu地图Application和存储一些公有的参数)
 * @date 2014-3-10 下午7:15:20
 */


//ACRA 崩溃临时配置
//@ReportsCrashes(
//        formUriBasicAuthLogin = "user",
//        formUriBasicAuthPassword = "password",
//        formUri = "http://117.27.234.237:8080/SSHI/afish/addAndroidCrashLog")


//Dialog
//@ReportsCrashes(
//        mode = ReportingInteractionMode.DIALOG,
//        resToastText = R.string.crash_toast_text, // optional, displayed as soon as the crash occurs, before collecting data which can take a few seconds
//        resDialogText = R.string.crash_dialog_text,
//        resDialogIcon = android.R.drawable.ic_dialog_info, //optional. default is a warning sign
//        resDialogTitle = R.string.crash_dialog_title, // optional. default is your application name
//        resDialogCommentPrompt = R.string.crash_dialog_comment_prompt, // optional. When defined, adds a user text field input with this text resource as a label
//        //resDialogEmailPrompt = R.string.crash_user_email_label, // optional. When defined, adds a user email text entry with this text resource as label. The email address will be populated from SharedPreferences and will be provided as an ACRA field if configured.
//        resDialogOkToast = R.string.crash_dialog_ok_toast // optional. displays a Toast message when the user accepts to send a report.
//)


//Status bar notification
//@ReportsCrashes(// Enter values from DIALOG section of wiki here!
//        resNotifTickerText = R.string.crash_notif_ticker_text,
//        resNotifTitle = R.string.crash_notif_title,
//        resNotifText = R.string.crash_notif_text,
//        resNotifIcon = android.R.drawable.stat_notify_error // optional. default is a warning sign
//)


//Toast 崩溃提醒
public class AFishApplication extends Application {

    private static AFishApplication appSelf = null;
    //百度key是否可用
    public boolean m_bKeyRight = true;
//    BMapManager mBMapManager = null;


    public static final String DB_NAME = "AppDb";
    public static final String strKey = "QuODhPFPf3g5V3mhnmt1epXR";


    @Override
    public void onCreate() {
        super.onCreate();
//        ACRA.init(this);
//        JwOwnSender yourSender = new JwOwnSender();
//        ACRA.getErrorReporter().setReportSender(yourSender);
//        isDownload = false;
        appSelf = this;
      //  initEngineManager(this);
        initImageLoader(getApplicationContext());
//        daoMaster = getDaoMaster(getApplicationContext());
//        daoSession = getDaoSession(getApplicationContext());
    }

//    public int getResToastText() {
////       return R.string.crash_toast_text;
//    }






    public  void initImageLoader(Context context) {
        // Create default options which will be used for every
        // displayImage(...) call if no options will be passed to this method
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .threadPoolSize(3)// default
                .threadPriority(Thread.NORM_PRIORITY - 1)// default
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .memoryCacheSizePercentage(13) // default
                .defaultDisplayImageOptions(defaultOptions)
                .writeDebugLogs() // Remove for release app
                .build();
        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config);
        getPackName();

    }

//    public void initEngineManager(Context context) {
//        if (mBMapManager == null) {
//            mBMapManager = new BMapManager(context);
//        }
//
//        if (!mBMapManager.init(strKey,new MyGeneralListener())) {
////            Toast.makeText(DemoApplication.getInstance().getApplicationContext(),
////                    "BMapManager  初始化错�?", Toast.LENGTH_LONG).show();
//        }
//    }

    public void initEngineManager(Context context) {
        // 在使用 SDK 各组间之前初始化 context 信息，传入 ApplicationContext
//        SDKInitializer.initialize(this);
    }

    public static AFishApplication getAppSelf() {
        return appSelf;
    }






    // 常用事件监听，用来处理�?常的网络错误，授权验证错误等
//    static class MyGeneralListener implements MKGeneralListener {
//
//        @Override
//        public void onGetNetworkState(int iError) {
//            if (iError == MKEvent.ERROR_NETWORK_CONNECT) {
////                Toast.makeText(DemoApplication.getInstance().getApplicationContext(), "您的网络出错啦！",
////                    Toast.LENGTH_LONG).show();
//            }
//            else if (iError == MKEvent.ERROR_NETWORK_DATA) {
////                Toast.makeText(DemoApplication.getInstance().getApplicationContext(), "输入正确的检索条件！",
////                        Toast.LENGTH_LONG).show();
//            }
//            // ...
//        }
//
//        @Override
//        public void onGetPermissionState(int iError) {
//            if (iError ==  MKEvent.ERROR_PERMISSION_DENIED) {
//                //授权Key错误�?
////                Toast.makeText(DemoApplication.getInstance().getApplicationContext(),
////                        "请在 DemoApplication.java文件输入正确的授权Key�?, Toast.LENGTH_LONG).show();
//                TiemaApplication.getInstance().m_bKeyRight = false;
//            }
//        }
//    }
    public void getPackName() {
//        new PackUtils(this).GetPackName();
    }
}