package com.afish.syl.lib.api.core.toast;

import android.app.Activity;
import android.widget.Toast;

import com.devspark.appmsg.AppMsg;
import com.afish.syl.lib.api.core.base.AFishApplication;
import com.afish.syl.lib.api.core.afishpublic.string.StrUtils;

import de.keyboardsurfer.android.widget.crouton.Configuration;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

/**
 * Created by 苏逸龙 on 2015-08-27.
 * 三种Toast弹出 提醒类
 * 1.原生
 * 2.crouton
 * 3.appmsg
 */
public class AFishToast {

    /**
     * MethodName:AppMsgShowALERT
     * Method description
     *
     *
     * @param act
     * @param msg
     */
    public static void AppMsgShowALERT(Activity act, String msg) {
        if (StrUtils.isEmpty(msg)) {
            return;
        }

        AppMsg.makeText(act, msg, AppMsg.STYLE_ALERT).show();
    }

    /**
     * MethodName:AppMsgShowCONFIRM
     * Method description
     *
     *
     * @param act
     * @param msg
     */
    public static void AppMsgShowCONFIRM(Activity act, String msg) {
        if (StrUtils.isEmpty(msg)) {
            return;
        }

        AppMsg.makeText(act, msg, AppMsg.STYLE_CONFIRM).show();
    }

    /**
     * MethodName:AppMsgShowINFO
     * Method description
     *
     *
     * @param act
     * @param msg
     */
    public static void AppMsgShowINFO(Activity act, String msg) {
        if (StrUtils.isEmpty(msg)) {
            return;
        }

        AppMsg.makeText(act, msg, AppMsg.STYLE_INFO).show();
    }

    /**
     * MethodName:CroutonShowALERT
     * Method description
     *
     *
     * @param act
     * @param msg
     */
    public static void CroutonShowALERT(Activity act, String msg) {
        if (StrUtils.isEmpty(msg)) {
            return;
        }

        Crouton.makeText(act, msg, Style.ALERT, Configuration.DURATION_LONG).show();
    }

    /**
     * MethodName:CroutonShowCONFIRM
     * Method description
     *
     *
     * @param act
     * @param msg
     */
    public static void CroutonShowCONFIRM(Activity act, String msg) {
        if (StrUtils.isEmpty(msg)) {
            return;
        }

        Crouton.makeText(act, msg, Style.CONFIRM, Configuration.DURATION_LONG).show();
    }

    /**
     * MethodName:CroutonShowINFO
     * Method description
     *
     *
     * @param act
     * @param msg
     */
    public static void CroutonShowINFO(Activity act, String msg) {
        if (StrUtils.isEmpty(msg)) {
            return;
        }

        Crouton.makeText(act, msg, Style.INFO, Configuration.DURATION_LONG).show();
    }

    /**
     * MethodName:JwToastTest
     * Method description
     *
     */
    public void JwToastTest() {
        Toast.makeText(AFishApplication.getAppSelf(), "aaaa", Toast.LENGTH_LONG).show();
    }

    /**
     * MethodName:ToastShow
     * Method description
     * 通过调度afishApplication最为Context直接显示吐司
     *
     * @param msg
     */
    public static void ToastShow(String msg) {
        if (StrUtils.isEmpty(msg)) {
            return;
        }

        Toast.makeText(AFishApplication.getAppSelf(), msg, Toast.LENGTH_LONG).show();
    }
}


//~ Formatted by syl