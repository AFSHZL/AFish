package com.afish.syl.lib.api.component.controller;

/**
 * Created by Administrator on 2015-08-11.
 */
import android.content.Context;

import android.util.AttributeSet;

import android.view.MotionEvent;

import android.widget.ScrollView;

/**
 * ClassName: VPScrollView
 * Class description
 * 有弹性的ScrollView 实现下拉弹回和上拉弹回
 *
 * @version        1.0, 16/01/27
 * @author         syl    
 */
public class VPScrollView extends ScrollView {

    // 滑动距离及坐标

    /** Field description */
    private float xDistance, yDistance, xLast, yLast;

    /**
     * Constructs ...
     *
     *
     * @param context
     * @param attrs
     */
    public VPScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);

        // TODO Auto-generated constructor stub
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {

        // TODO Auto-generated method stub
        switch (ev.getAction()) {
        case MotionEvent.ACTION_DOWN :
            xDistance = yDistance = 0f;
            xLast     = ev.getX();
            yLast     = ev.getY();

            break;

        case MotionEvent.ACTION_MOVE :
            final float curX = ev.getX();
            final float curY = ev.getY();

            xDistance += Math.abs(curX - xLast);
            yDistance += Math.abs(curY - yLast);
            xLast     = curX;
            yLast     = curY;

            if (xDistance > yDistance) {
                return false;
            }
        }

        return super.onInterceptTouchEvent(ev);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
