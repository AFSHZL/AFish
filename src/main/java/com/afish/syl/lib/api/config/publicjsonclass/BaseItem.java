package com.afish.syl.lib.api.config.publicjsonclass;

import java.io.Serializable;

/**
 * 基础bean
 * 
 * @author hjixiang@linewell.com
 * @sine 2014-11-14
 */
public class BaseItem implements Serializable {

	/**
	 * 序列号标识
	 */
	private static final long serialVersionUID = 3525742594593514676L;
	
}
