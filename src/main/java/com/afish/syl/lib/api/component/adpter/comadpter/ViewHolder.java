package com.afish.syl.lib.api.component.adpter.comadpter;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afish.syl.lib.api.core.afishpublic.o.OUtils;
import com.afish.syl.lib.api.core.afishpublic.string.StrUtils;
import com.afish.syl.lib.api.core.log.AFishLog;

/**
 * ClassName: ViewHolder
 * Class description
 * 操作控件
 *
 * @version        1.0, 16/01/27
 * @author         syl    
 */
public class ViewHolder {

    /** Field View */
    private final SparseArray<View> mViews;

    /** Field 第几项 */
    private int mPosition;

    /** Field 底层View */
    private View mConvertView;

    private ViewHolder(Context context, ViewGroup parent, int layoutId, int position) {
        this.mPosition = position;
        this.mViews    = new SparseArray<View>();
        mConvertView   = LayoutInflater.from(context).inflate(layoutId, parent, false);

        // setTag
        mConvertView.setTag(this);
    }

    /**
     * MethodName:getConvertView
     * Method description
     * 获取布局底层View
	 *
	 * @return
	 */
    public View getConvertView() {
        return mConvertView;
    }

    /**
     * 拿到一个ViewHolder对象
     *
     * @param context
     * @param convertView
     * @param parent
     * @param layoutId
     * @param position
     * @return
     */
    public static ViewHolder get(Context context, View convertView, ViewGroup parent, int layoutId, int position) {
        if (convertView == null) {
            return new ViewHolder(context, parent, layoutId, position);
        }

        return (ViewHolder) convertView.getTag();
    }

    /**
     * 为ImageView设置图片
     *
     * @param viewId
     * @param bm
     * @return
     */
    public ViewHolder setImageBitmap(int viewId, Bitmap bm) {
        ImageView view = getView(viewId);

        if (OUtils.IsNotNull(view)) {
            try {
                view.setImageBitmap(bm);
            } catch (Exception e) {
                AFishLog.e(e.getMessage());
            }
        } else {
            AFishLog.e("没有获取到View ID:" + viewId);
        }

        return this;
    }

    /**
     * 为ImageView设置图片
     *
     * @param viewId
     * @param url
     * @return
     */
    public ViewHolder setImageByUrl(int viewId, String url) {
        ImageView view = getView(viewId);

        if (OUtils.IsNotNull(view)) {
            try {
                ImageLoader.getInstance(3, ImageLoader.Type.LIFO).loadImage(url, view);
            } catch (Exception e) {
                AFishLog.e(e.getMessage());
            }
        } else {
            AFishLog.e("没有获取到View ID:" + viewId);
        }

        return this;
    }

    /**
     * 为ImageView设置图片
     *
     * @param viewId
     * @param drawableId
     * @return
     */
    public ViewHolder setImageResource(int viewId, int drawableId) {
        ImageView view = getView(viewId);

        if (OUtils.IsNotNull(view)) {
            try {
                view.setImageResource(drawableId);
            } catch (Exception e) {
                AFishLog.e(e.getMessage());
            }
        } else {
            AFishLog.e("没有获取到View ID:" + viewId + " drawableId:" + drawableId);
        }

        return this;
    }

    /**
     * 获取ImageView
     *
     * @param viewId
     * @return
     */
    public ImageView getImageView(int viewId) {
        ImageView view = getView(viewId);

        return view;
    }

    /**
     * MethodName:getPosition
     * Method description
     *
     *
     * @return
     */
    public int getPosition() {
        return mPosition;
    }

    /**
     * 为TextView设置字符串
     *
     * @param viewId
     * @param text
     * @return
     */
    public ViewHolder setText(int viewId, String text) {
        TextView view = getView(viewId);

        if (OUtils.IsNotNull(view)) {
            if (StrUtils.IsNotEmpty(text)) {
                view.setText(text);
            }
        } else {
            AFishLog.e("没有获取到View ID:" + viewId + " Text:" + text);
        }

        return this;
    }

    /**
     * 通过控件的Id获取对于的控件，如果没有则加入views
     *
     * @param viewId
     * @param <T>
     * @return
     */
    public <T extends View> T getView(int viewId) {
        View view = mViews.get(viewId);

        if (view == null) {
            view = mConvertView.findViewById(viewId);
            mViews.put(viewId, view);
        }

        return (T) view;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
