package com.afish.syl.lib.api.core.control.imageloader;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.afish.syl.lib.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;


/**
 * @author suyilong     317616660@qq.com
 * @ClassName: SylImageLoaderUtil
 * @Description: TODO(universalimageloader二次封装Static快捷下载并缓存图片)
 * @date 2014-3-10 下午7:05:36
 */
public class AFishImageLoader {

    /**
     * @Fields imageLoader : TODO(实例话ImageLoader对象)
     */
    private static ImageLoader imageLoader = ImageLoader.getInstance();

    /**
     * @Fields options : TODO(下载前的图片配置（占位图，空图，失败图)
     */
    private static DisplayImageOptions options;

    static {}

    static {}

    /**
     * @Title: displayImage
     * @Description: TODO(图片快捷下载并缓存)
     */
    public static void displayImage(String uri, ImageView imageView) {
        if ((uri != null) && (uri.length() > 0)) {
            options = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.aio_image_default)
                                                       .showImageForEmptyUri(R.drawable.aio_image_default_round)
                                                       .showImageOnFail(R.drawable.aio_image_fail)
                                                       .cacheInMemory(true)
                                                       .cacheOnDisc(true)
                                                       .considerExifParams(true)
                                                       .bitmapConfig(Bitmap.Config.RGB_565)
                                                       .build();
            imageLoader.displayImage(uri, imageView, options);
        }
    }

    /**
     * MethodName:getImageLoader
     * Method description
     * 获取静态的图片加载
     *
     * @return
     */
    public static ImageLoader getImageLoader() {
        return imageLoader;
    }
}


//~ Formatted by syl