package com.afish.syl.lib.api.config.publicjsonclass;

/**
 * Created by Administrator on 2014-12-24.
 */
public class ReportRoleItem {
    private String code;
    private String action_name;
    private Integer is_show;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAction_name() {
        return action_name;
    }

    public void setAction_name(String action_name) {
        this.action_name = action_name;
    }

    public Integer getIs_show() {
        return is_show;
    }

    public void setIs_show(Integer is_show) {
        this.is_show = is_show;
    }
}
