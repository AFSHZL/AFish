package com.afish.syl.lib.api.config.publicjsonclass;

import java.util.ArrayList;
import java.util.List;

public class LoadProductItem {
    String name;
    String content;
    String flag;
    String htmlurl;
    int categoryid;
    int productid;
    String categoryname;
    List<PicPathItem> piclist = new ArrayList<PicPathItem>();
    double integral;
    int picsum;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getHtmlurl() {
        return htmlurl;
    }

    public void setHtmlurl(String htmlurl) {
        this.htmlurl = htmlurl;
    }

    public int getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(int categoryid) {
        this.categoryid = categoryid;
    }

    public int getProductid() {
        return productid;
    }

    public void setProductid(int productid) {
        this.productid = productid;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public List<PicPathItem> getPiclist() {
        return piclist;
    }

    public void setPiclist(List<PicPathItem> piclist) {
        this.piclist = piclist;
    }

    public double getIntegral() {
        return integral;
    }

    public void setIntegral(double integral) {
        this.integral = integral;
    }

    public int getPicsum() {
        return picsum;
    }

    public void setPicsum(int picsum) {
        this.picsum = picsum;
    }
}
