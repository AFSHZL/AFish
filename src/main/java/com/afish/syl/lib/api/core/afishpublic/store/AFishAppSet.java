package com.afish.syl.lib.api.core.afishpublic.store;

import android.content.Context;
import android.content.SharedPreferences;

import com.afish.syl.lib.api.core.base.AFishApplication;

import java.util.Set;

/**
 * AppSetting 快速App配置 轻数据缓存
 * Created by 苏逸龙 on 2015-03-08.
 */
public class AFishAppSet {

    /** Field 保存本地的XML */
    private static String sAppSet = "appSet";

    /**
     * 写入布尔
     *
     * @param key
     * @param value
     */
    public static void putBoolean(String key, boolean value) {
        putBoolean(AFishApplication.getAppSelf(), key, value);
    }

    /**
     * 写入布尔
     *
     * @param context
     * @param key
     * @param value
     */
    public static void putBoolean(Context context, String key, boolean value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(sAppSet, 0);

        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    /**
     * 写入浮点
     *
     * @param key
     * @param value
     */
    public static void putFloat(String key, float value) {
        putFloat(AFishApplication.getAppSelf(), key, value);
    }

    /**
     * 写入长整
     *
     * @param key
     * @param value
     */
    public static void putFloat(String key, long value) {
        putLong(AFishApplication.getAppSelf(), key, value);
    }

    /**
     * 写入浮点
     *
     * @param context
     * @param key
     * @param value
     */
    public static void putFloat(Context context, String key, float value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(sAppSet, 0);

        sharedPreferences.edit().putFloat(key, value).apply();
    }

    /**
     * 写入整型
     *
     * @param key
     * @param value
     */
    public static void putInt(String key, int value) {
        putInt(AFishApplication.getAppSelf(), key, value);
    }

    /**
     * 写入整型
     *
     * @param context
     * @param key
     * @param value
     */
    public static void putInt(Context context, String key, int value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(sAppSet, 0);

        sharedPreferences.edit().putInt(key, value).apply();
    }

    /**
     * 写入长整
     *
     * @param context
     * @param key
     * @param value
     */
    public static void putLong(Context context, String key, long value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(sAppSet, 0);

        sharedPreferences.edit().putLong(key, value).apply();
    }

    /**
     * 写入文本到配置中
     *
     * @param key
     * @param value
     */
    public static void putString(String key, String value) {
        putString(AFishApplication.getAppSelf(), key, value);
    }

    /**
     * 写入文本到配置中
     *
     * @param context
     * @param key
     * @param value
     */
    public static void putString(Context context, String key, String value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(sAppSet, 0);

        sharedPreferences.edit().putString(key, value).apply();
    }

    /**
     * 写入文本集合
     *
     * @param key
     * @param value
     *
     * @return
     */
    public static boolean putStringSet(String key, Set<String> value) {
        return putStringSet(AFishApplication.getAppSelf(), key, value);
    }

    /**
     * 写入文本集合
     *
     * @param context
     * @param key
     * @param value
     *
     * @return
     */
    public static boolean putStringSet(Context context, String key, Set<String> value) {
        if ((value != null) && (value.size() > 0)) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(sAppSet, 0);

            sharedPreferences.edit().putStringSet(key, value).apply();

            return true;
        } else {
            return false;
        }
    }

    /**
     * 布尔
     *
     * @param sKey
     * @return
     */
    public static boolean getBoolean(String sKey) {
        return getBoolean(AFishApplication.getAppSelf(), sKey);
    }

    /**
     * 布尔
     *
     * @param context
     * @param string
     * @return
     */
    public static boolean getBoolean(Context context, String string) {
        boolean           resStr            = false;
        SharedPreferences sharedPreferences = context.getSharedPreferences(sAppSet, 0);

        resStr = sharedPreferences.getBoolean(string, false);

        return resStr;
    }

    /**
     * 获取浮点
     *
     * @param sKey
     * @return
     */
    public static float getFloat(String sKey) {
        return getFloat(AFishApplication.getAppSelf(), sKey);
    }

    /////

    /**
     * 获取浮点
     *
     * @param context
     * @param string
     * @return
     */
    public static float getFloat(Context context, String string) {
        float             resStr            = 0;
        SharedPreferences sharedPreferences = context.getSharedPreferences(sAppSet, 0);

        resStr = sharedPreferences.getFloat(string, 0);

        return resStr;
    }

    /**
     * 获取整型
     *
     * @param sKey
     * @return
     */
    public static int getInt(String sKey) {
        return getInt(AFishApplication.getAppSelf(), sKey);
    }

    /**
     * 获取整型
     *
     * @param context
     * @param string
     * @return
     */
    public static int getInt(Context context, String string) {
        int               resStr            = 0;
        SharedPreferences sharedPreferences = context.getSharedPreferences(sAppSet, 0);

        resStr = sharedPreferences.getInt(string, 0);

        return resStr;
    }

    /**
     * 获取长整
     *
     * @param sKey
     * @return
     */
    public static float getLong(String sKey) {
        return getLong(AFishApplication.getAppSelf(), sKey);
    }

    /////

    /**
     * 获取长整
     *
     * @param context
     * @param string
     * @return
     */
    public static float getLong(Context context, String string) {
        float             resStr            = 0;
        SharedPreferences sharedPreferences = context.getSharedPreferences(sAppSet, 0);

        resStr = sharedPreferences.getLong(string, 0);

        return resStr;
    }

    /**
     * 获取配置中的文本
     *
     * @param sKey
     * @return
     */
    public static String getString(String sKey) {
        return getString(AFishApplication.getAppSelf(), sKey);
    }

    /**
     * MethodName:getString
     * Method description
     *
     *
     * @param context
     * @param string
     *
     * @return
     */
    public static String getString(Context context, String string) {
        String            resStr            = "";
        SharedPreferences sharedPreferences = context.getSharedPreferences(sAppSet, 0);

        resStr = sharedPreferences.getString(string, "");

        return resStr;
    }

    /**
     * 获取文本集合
     *
     * @param sKey
     * @return
     */
    public static Set<String> getStringSet(String sKey) {
        return getStringSet(AFishApplication.getAppSelf(), sKey);
    }

//  ///

    /**
     * 获取文本集合
     *
     * @param context
     * @param string
     * @return
     */
    public static Set<String> getStringSet(Context context, String string) {
        Set<String>       resStr            = null;
        SharedPreferences sharedPreferences = context.getSharedPreferences(sAppSet, 0);

        resStr = sharedPreferences.getStringSet(string, null);

        return resStr;
    }
}


//~ Formatted by syl