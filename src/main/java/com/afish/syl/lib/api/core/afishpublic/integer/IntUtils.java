package com.afish.syl.lib.api.core.afishpublic.integer;

/**
 * 作者：苏逸龙 on 2015-10-27 14:49
 * QQ：317616660
 */
public class IntUtils {

    /**
     * MethodName:toStr
     * Method description
     * 整型转字符串
     *
     * @param value
     *
     * @return
     */
    public static String toStr(int value) {
        return IntUtils.toStr(value, "");
    }

    /**
     * MethodName:toStr
     * Method description
     *
     *
     * @param value
     * @param sDefValue
     *
     * @return
     */
    public static String toStr(int value, String sDefValue) {
        try {
            return Integer.valueOf(value).toString();
        } catch (Exception e) {}

        return sDefValue;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
