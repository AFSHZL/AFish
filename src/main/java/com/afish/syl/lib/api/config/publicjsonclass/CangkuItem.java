package com.afish.syl.lib.api.config.publicjsonclass;

/**
 * Created by Administrator on 2014-12-31.
 */
public class CangkuItem {
    String ckdm;
    String ckmc;

    public String getCkdm() {
        return ckdm;
    }

    public void setCkdm(String ckdm) {
        this.ckdm = ckdm;
    }

    public String getCkmc() {
        return ckmc;
    }

    public void setCkmc(String ckmc) {
        this.ckmc = ckmc;
    }
}
