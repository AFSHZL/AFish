package com.afish.syl.lib.api.core.afishpublic.o;

/**
 * Created by Administrator on 2015-08-18.
 * Object应用单元
 */
public class OUtils {

    /**
     * MethodName:IsNotNull
     * Method description
     * 判断对象不为空
     *
     * @param o
     *
     * @return
     */
    public static boolean IsNotNull(Object o) {
        return o != null;
    }

    /**
     * MethodName:IsNull
     * Method description
     * 判断对象空
     *
     * @param o
     *
     * @return
     */
    public static boolean IsNull(Object o) {
        return !IsNotNull(o);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
