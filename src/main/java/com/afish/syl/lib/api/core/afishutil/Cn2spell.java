package com.afish.syl.lib.api.core.afishutil;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

public class Cn2spell {
	private static LinkedHashMap spellmap = null;

	static {
		if (spellmap == null) {
			spellmap = new LinkedHashMap(400);
		}
		initialize();
		System.out.println("chinese transfer spell done.");
	}

	private Cn2spell() {
	}

	private static void spellput(String spell, int ascii) {
		spellmap.put(spell, new Integer(ascii));
	}

	private static void initialize() {
		spellput("a", -20319);
		spellput("ai", -20317);
		spellput("an", -20304);
		spellput("ang", -20295);
		spellput("ao", -20292);
		spellput("ba", -20283);
		spellput("bai", -20265);
		spellput("ban", -20257);
		spellput("bang", -20242);
		spellput("bao", -20230);
		spellput("bei", -20051);
		spellput("ben", -20036);
		spellput("beng", -20032);
		spellput("bi", -20026);
		spellput("bian", -20002);
		spellput("biao", -19990);
		spellput("bie", -19986);
		spellput("bin", -19982);
		spellput("bing", -19976);
		spellput("bo", -19805);
		spellput("bu", -19784);
		spellput("ca", -19775);
		spellput("cai", -19774);
		spellput("can", -19763);
		spellput("cang", -19756);
		spellput("cao", -19751);
		spellput("ce", -19746);
		spellput("ceng", -19741);
		spellput("cha", -19739);
		spellput("chai", -19728);
		spellput("chan", -19725);
		spellput("chang", -19715);
		spellput("chao", -19540);
		spellput("che", -19531);
		spellput("chen", -19525);
		spellput("cheng", -19515);
		spellput("chi", -19500);
		spellput("chong", -19484);
		spellput("chou", -19479);
		spellput("chu", -19467);
		spellput("chuai", -19289);
		spellput("chuan", -19288);
		spellput("chuang", -19281);
		spellput("chui", -19275);
		spellput("chun", -19270);
		spellput("chuo", -19263);
		spellput("ci", -19261);
		spellput("cong", -19249);
		spellput("cou", -19243);
		spellput("cu", -19242);
		spellput("cuan", -19238);
		spellput("cui", -19235);
		spellput("cun", -19227);
		spellput("cuo", -19224);
		spellput("da", -19218);
		spellput("dai", -19212);
		spellput("dan", -19038);
		spellput("dang", -19023);
		spellput("dao", -19018);
		spellput("de", -19006);
		spellput("deng", -19003);
		spellput("di", -18996);
		spellput("dian", -18977);
		spellput("diao", -18961);
		spellput("die", -18952);
		spellput("ding", -18783);
		spellput("diu", -18774);
		spellput("dong", -18773);
		spellput("dou", -18763);
		spellput("du", -18756);
		spellput("duan", -18741);
		spellput("dui", -18735);
		spellput("dun", -18731);
		spellput("duo", -18722);
		spellput("e", -18710);
		spellput("en", -18697);
		spellput("er", -18696);
		spellput("fa", -18526);
		spellput("fan", -18518);
		spellput("fang", -18501);
		spellput("fei", -18490);
		spellput("fen", -18478);
		spellput("feng", -18463);
		spellput("fo", -18448);
		spellput("fou", -18447);
		spellput("fu", -18446);
		spellput("ga", -18239);
		spellput("gai", -18237);
		spellput("gan", -18231);
		spellput("gang", -18220);
		spellput("gao", -18211);
		spellput("ge", -18201);
		spellput("gei", -18184);
		spellput("gen", -18183);
		spellput("geng", -18181);
		spellput("gong", -18012);
		spellput("gou", -17997);
		spellput("gu", -17988);
		spellput("gua", -17970);
		spellput("guai", -17964);
		spellput("guan", -17961);
		spellput("guang", -17950);
		spellput("gui", -17947);
		spellput("gun", -17931);
		spellput("guo", -17928);
		spellput("ha", -17922);
		spellput("hai", -17759);
		spellput("han", -17752);
		spellput("hang", -17733);
		spellput("hao", -17730);
		spellput("he", -17721);
		spellput("hei", -17703);
		spellput("hen", -17701);
		spellput("heng", -17697);
		spellput("hong", -17692);
		spellput("hou", -17683);
		spellput("hu", -17676);
		spellput("hua", -17496);
		spellput("huai", -17487);
		spellput("huan", -17482);
		spellput("huang", -17468);
		spellput("hui", -17454);
		spellput("hun", -17433);
		spellput("huo", -17427);
		spellput("ji", -17417);
		spellput("jia", -17202);
		spellput("jian", -17185);
		spellput("jiang", -16983);
		spellput("jiao", -16970);
		spellput("jie", -16942);
		spellput("jin", -16915);
		spellput("jing", -16733);
		spellput("jiong", -16708);
		spellput("jiu", -16706);
		spellput("ju", -16689);
		spellput("juan", -16664);
		spellput("jue", -16657);
		spellput("jun", -16647);
		spellput("ka", -16474);
		spellput("kai", -16470);
		spellput("kan", -16465);
		spellput("kang", -16459);
		spellput("kao", -16452);
		spellput("ke", -16448);
		spellput("ken", -16433);
		spellput("keng", -16429);
		spellput("kong", -16427);
		spellput("kou", -16423);
		spellput("ku", -16419);
		spellput("kua", -16412);
		spellput("kuai", -16407);
		spellput("kuan", -16403);
		spellput("kuang", -16401);
		spellput("kui", -16393);
		spellput("kun", -16220);
		spellput("kuo", -16216);
		spellput("la", -16212);
		spellput("lai", -16205);
		spellput("lan", -16202);
		spellput("lang", -16187);
		spellput("lao", -16180);
		spellput("le", -16171);
		spellput("lei", -16169);
		spellput("leng", -16158);
		spellput("li", -16155);
		spellput("lia", -15959);
		spellput("lian", -15958);
		spellput("liang", -15944);
		spellput("liao", -15933);
		spellput("lie", -15920);
		spellput("lin", -15915);
		spellput("ling", -15903);
		spellput("liu", -15889);
		spellput("long", -15878);
		spellput("lou", -15707);
		spellput("lu", -15701);
		spellput("lv", -15681);
		spellput("luan", -15667);
		spellput("lue", -15661);
		spellput("lun", -15659);
		spellput("luo", -15652);
		spellput("ma", -15640);
		spellput("mai", -15631);
		spellput("man", -15625);
		spellput("mang", -15454);
		spellput("mao", -15448);
		spellput("me", -15436);
		spellput("mei", -15435);
		spellput("men", -15419);
		spellput("meng", -15416);
		spellput("mi", -15408);
		spellput("mian", -15394);
		spellput("miao", -15385);
		spellput("mie", -15377);
		spellput("min", -15375);
		spellput("ming", -15369);
		spellput("miu", -15363);
		spellput("mo", -15362);
		spellput("mou", -15183);
		spellput("mu", -15180);
		spellput("na", -15165);
		spellput("nai", -15158);
		spellput("nan", -15153);
		spellput("nang", -15150);
		spellput("nao", -15149);
		spellput("ne", -15144);
		spellput("nei", -15143);
		spellput("nen", -15141);
		spellput("neng", -15140);
		spellput("ni", -15139);
		spellput("nian", -15128);
		spellput("niang", -15121);
		spellput("niao", -15119);
		spellput("nie", -15117);
		spellput("nin", -15110);
		spellput("ning", -15109);
		spellput("niu", -14941);
		spellput("nong", -14937);
		spellput("nu", -14933);
		spellput("nv", -14930);
		spellput("nuan", -14929);
		spellput("nue", -14928);
		spellput("nuo", -14926);
		spellput("o", -14922);
		spellput("ou", -14921);
		spellput("pa", -14914);
		spellput("pai", -14908);
		spellput("pan", -14902);
		spellput("pang", -14894);
		spellput("pao", -14889);
		spellput("pei", -14882);
		spellput("pen", -14873);
		spellput("peng", -14871);
		spellput("pi", -14857);
		spellput("pian", -14678);
		spellput("piao", -14674);
		spellput("pie", -14670);
		spellput("pin", -14668);
		spellput("ping", -14663);
		spellput("po", -14654);
		spellput("pu", -14645);
		spellput("qi", -14630);
		spellput("qia", -14594);
		spellput("qian", -14429);
		spellput("qiang", -14407);
		spellput("qiao", -14399);
		spellput("qie", -14384);
		spellput("qin", -14379);
		spellput("qing", -14368);
		spellput("qiong", -14355);
		spellput("qiu", -14353);
		spellput("qu", -14345);
		spellput("quan", -14170);
		spellput("que", -14159);
		spellput("qun", -14151);
		spellput("ran", -14149);
		spellput("rang", -14145);
		spellput("rao", -14140);
		spellput("re", -14137);
		spellput("ren", -14135);
		spellput("reng", -14125);
		spellput("ri", -14123);
		spellput("rong", -14122);
		spellput("rou", -14112);
		spellput("ru", -14109);
		spellput("ruan", -14099);
		spellput("rui", -14097);
		spellput("run", -14094);
		spellput("ruo", -14092);
		spellput("sa", -14090);
		spellput("sai", -14087);
		spellput("san", -14083);
		spellput("sang", -13917);
		spellput("sao", -13914);
		spellput("se", -13910);
		spellput("sen", -13907);
		spellput("seng", -13906);
		spellput("sha", -13905);
		spellput("shai", -13896);
		spellput("shan", -13894);
		spellput("shang", -13878);
		spellput("shao", -13870);
		spellput("she", -13859);
		spellput("shen", -13847);
		spellput("sheng", -13831);
		spellput("shi", -13658);
		spellput("shou", -13611);
		spellput("shu", -13601);
		spellput("shua", -13406);
		spellput("shuai", -13404);
		spellput("shuan", -13400);
		spellput("shuang", -13398);
		spellput("shui", -13395);
		spellput("shun", -13391);
		spellput("shuo", -13387);
		spellput("si", -13383);
		spellput("song", -13367);
		spellput("sou", -13359);
		spellput("su", -13356);
		spellput("suan", -13343);
		spellput("sui", -13340);
		spellput("sun", -13329);
		spellput("suo", -13326);
		spellput("ta", -13318);
		spellput("tai", -13147);
		spellput("tan", -13138);
		spellput("tang", -13120);
		spellput("tao", -13107);
		spellput("te", -13096);
		spellput("teng", -13095);
		spellput("ti", -13091);
		spellput("tian", -13076);
		spellput("tiao", -13068);
		spellput("tie", -13063);
		spellput("ting", -13060);
		spellput("tong", -12888);
		spellput("tou", -12875);
		spellput("tu", -12871);
		spellput("tuan", -12860);
		spellput("tui", -12858);
		spellput("tun", -12852);
		spellput("tuo", -12849);
		spellput("wa", -12838);
		spellput("wai", -12831);
		spellput("wan", -12829);
		spellput("wang", -12812);
		spellput("wei", -12802);
		spellput("wen", -12607);
		spellput("weng", -12597);
		spellput("wo", -12594);
		spellput("wu", -12585);
		spellput("xi", -12556);
		spellput("xia", -12359);
		spellput("xian", -12346);
		spellput("xiang", -12320);
		spellput("xiao", -12300);
		spellput("xie", -12120);
		spellput("xin", -12099);
		spellput("xing", -12089);
		spellput("xiong", -12074);
		spellput("xiu", -12067);
		spellput("xu", -12058);
		spellput("xuan", -12039);
		spellput("xue", -11867);
		spellput("xun", -11861);
		spellput("ya", -11847);
		spellput("yan", -11831);
		spellput("yang", -11798);
		spellput("yao", -11781);
		spellput("ye", -11604);
		spellput("yi", -11589);
		spellput("yin", -11536);
		spellput("ying", -11358);
		spellput("yo", -11340);
		spellput("yong", -11339);
		spellput("you", -11324);
		spellput("yu", -11303);
		spellput("yuan", -11097);
		spellput("yue", -11077);
		spellput("yun", -11067);
		spellput("za", -11055);
		spellput("zai", -11052);
		spellput("zan", -11045);
		spellput("zang", -11041);
		spellput("zao", -11038);
		spellput("ze", -11024);
		spellput("zei", -11020);
		spellput("zen", -11019);
		spellput("zeng", -11018);
		spellput("zha", -11014);
		spellput("zhai", -10838);
		spellput("zhan", -10832);
		spellput("zhang", -10815);
		spellput("zhao", -10800);
		spellput("zhe", -10790);
		spellput("zhen", -10780);
		spellput("zheng", -10764);
		spellput("zhi", -10587);
		spellput("zhong", -10544);
		spellput("zhou", -10533);
		spellput("zhu", -10519);
		spellput("zhua", -10331);
		spellput("zhuai", -10329);
		spellput("zhuan", -10328);
		spellput("zhuang", -10322);
		spellput("zhui", -10315);
		spellput("zhun", -10309);
		spellput("zhuo", -10307);
		spellput("zi", -10296);
		spellput("zong", -10281);
		spellput("zou", -10274);
		spellput("zu", -10270);
		spellput("zuan", -10262);
		spellput("zui", -10260);
		spellput("zun", -10256);
		spellput("zuo", -10254);
	}

	/**
	 * 获得单个汉字的ascii.
	 * 
	 * @param cn
	 *            char 汉字字符
	 * @return int 错误返回 0,否则返回ascii
	 */
	public static int getcnascii(char cn) {
		try {
			byte[] bytes = (String.valueOf(cn)).getBytes("GBK");
		
		if (bytes == null || bytes.length > 2 || bytes.length <= 0) { // 错误
			return 0;
		}
		if (bytes.length == 1) { // 英文字符
			return bytes[0];
		}
		if (bytes.length == 2) { // 中文字符
			int hightbyte = 256 + bytes[0];
			int lowbyte = 256 + bytes[1];

			int ascii = (256 * hightbyte + lowbyte) - 256 * 256;

			// System.out.println("ascii=" + ascii);

			return ascii;
		}
		} catch (Exception e) {
			return 0;
		}
		return 0; // 错误
	}

	/**
	 * 根据ascii码到spellmap中查找对应的拼音
	 * 
	 * @param ascii
	 *            int 字符对应的ascii
	 * @return String 拼音,首先判断ascii是否>0&<160,如果是返回对应的字符, <br>
	 *         否则到spellmap中查找,如果没有找到拼音,则返回null,如果找到则返回拼音.
	 */
	public static String getspellbyascii(int ascii) {
		if (ascii > 0 && ascii < 160) { // 单字符
			return String.valueOf((char) ascii);
		}

		if (ascii < -20319 || ascii > -10247) { // 不知道的字符
			return null;
		}

		Set keyset = spellmap.keySet();
		Iterator it = keyset.iterator();

		String spell0 = null;
		;
		String spell = null;

		int asciirang0 = -20319;
		int asciirang;
		while (it.hasNext()) {

			spell = (String) it.next();
			Object valobj = spellmap.get(spell);
			if (valobj instanceof Integer) {
				asciirang = ((Integer) valobj).intValue();

				if (ascii >= asciirang0 && ascii < asciirang) { // 区间找到
					return (spell0 == null) ? spell : spell0;
				} else {
					spell0 = spell;
					asciirang0 = asciirang;
				}
			}
		}

		return null;

	}

	/**
	 * 返回字符串的全拼,是汉字转化为全拼,其他字符不进行转换
	 * 
	 * @param cnstr
	 *            String 字符串
	 * @return String 转换成全拼后的字符串
	 */
	public static String getfullspell(String cnstr) {
		if (null == cnstr || "".equals(cnstr.trim())) {
			return cnstr;
		}
		

		char[] chars = cnstr.toCharArray();
		StringBuffer retubuf = new StringBuffer();
		for (int i = 0, len = chars.length; i < len; i++) {

			if("铮".equals(chars[i])){

				retubuf.append("zheng");
				break;
			}
			if("炜".equals(chars[i])){

				retubuf.append("wei");
				break;
			}
			if("鑫".equals(chars[i])){

				retubuf.append("xin");
				break;
			}
			if("晏".equals(chars[i])){

				retubuf.append("yan");
				break;
			}
			if("菁".equals(chars[i])){

				retubuf.append("jing");
				break;
			}
			
			int ascii = getcnascii(chars[i]);
			if (ascii == 0) { // 取ascii时出错
				retubuf.append(chars[i]);
			} else {
				String spell = getspellbyascii(ascii);
				if (spell == null) {
					retubuf.append(chars[i]);
				} else {
					retubuf.append(spell);
				} // end of if spell == null
			} // end of if ascii <= -20400
		} // end of for

		return retubuf.toString();
	}

	public static String getfirstspell(String cnstr) {
		return null;
	}

	public static void main(String[] args) {
//		String str = null;
//		str = "谢海101普降喜雨铮";
//		System.out.println("spell=" + Cn2spell.getfullspell(str));
//
//		str = "张牙舞爪》。，";
//		System.out.println("spell=" + Cn2spell.getfullspell(str));
//
//		str = "李鹏，可耻下场。";
//		System.out.println("spell=" + Cn2spell.getfullspell(str));
//
//		str = "猪油，猪八戒。";
//		System.out.println("spell=" + Cn2spell.getfullspell(str));
	}
}
