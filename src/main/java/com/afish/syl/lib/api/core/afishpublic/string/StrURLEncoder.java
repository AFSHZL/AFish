package com.afish.syl.lib.api.core.afishpublic.string;

import java.io.UnsupportedEncodingException;

import java.net.URLEncoder;

/**
 * 快速Url编码类
 * 作者：苏逸龙 on 2015-08-28 10:51
 * QQ：317616660
 *
 */
public class StrURLEncoder {

    /**
     * MethodName:encode
     * Method description
     * URL加密
     *
     * @param s
     *
     * @return
     */
    public static String encode(String s) {
        String resStr = null;
        //开始加密
        try {
            resStr = StrUtils.IsNull(URLEncoder.encode(s, "utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (resStr == null) {
            resStr = "";
        }

        return resStr;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
