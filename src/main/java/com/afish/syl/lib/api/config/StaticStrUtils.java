package com.afish.syl.lib.api.config;

/**
 * 静态常用字符串
 * 由于有一些比较特殊的字符串写在strings.xml 里面并不方便
 * 所以写在这个类里
 * 作者：苏逸龙 on 2015-08-31 09:37
 * QQ：317616660
 */
public class StaticStrUtils {
    //静态程序Crash报错地址
    public static String CrashUrl = "http://117.27.234.237:8080/hospital180/afish/addAndroidCrashLog";
    //html url地址
    public static String htmlurl = "htmlurl";
    //html标题
    public static String title = "title";
    public static String pushid = "pushid";
    //正常定位
    public static String loc = "loc";
    public static String locstop = "locstop";
    //只定位一次
    public static String locone = "locone";

    public static String openmenu = "openmenu";

    //关闭所有预约页面
    public static String closeyuyue = "closeyuyue";
    public static String msg = "msg";
    public static String refresh = "refresh";

    public static String yyxz3 = "http://mp.weixin.qq.com/s?__biz=MzAwMjUyNDk1Nw==&mid=252104832&idx=1&sn=a8831af4b593ae5c1f3297c0bf21b740&scene=18#rd";
    public static String wxQuery = "http://www.180yy.com:8443/Exam_Lab_Qry/wxQuery.jsp";
    public static String hosNote = "http://36.249.60.46/hospitalPage.jsp";
    public static String hosnotice = "http://36.249.60.46/notice.jsp";
    public static String trafficRoute = "http://36.249.60.46/trafficRoute.jsp";
    public static String jzzn = "http://mp.weixin.qq.com/s?__biz=MzAwMjUyNDk1Nw==&mid=252104832&idx=2&sn=e623ff5e402515de05330cf5917673a2&scene=18&ptlang=2052&ADUIN=317616660&ADSESSION=1442538079&ADTAG=CLIENT.QQ.5431_.0&ADPUBNO=26510#rd";
    public static String yyxz = "http://mp.weixin.qq.com/s?__biz=MzAwMjUyNDk1Nw==&mid=252104832&idx=1&sn=a8831af4b593ae5c1f3297c0bf21b740&scene=18#rd";
    public static String yyxz1 = "http://mp.weixin.qq.com/s?__biz=MzAwMjUyNDk1Nw==&mid=252104832&idx=1&sn=a8831af4b593ae5c1f3297c0bf21b740&scene=18#rd";
    public static String yyxz2 = "http://mp.weixin.qq.com/s?__biz=MzAwMjUyNDk1Nw==&mid=252104832&idx=1&sn=a8831af4b593ae5c1f3297c0bf21b740&scene=18#rd";
    public static String yyxz5 = "http://mp.weixin.qq.com/s?__biz=MzAwMjUyNDk1Nw==&mid=252104832&idx=1&sn=a8831af4b593ae5c1f3297c0bf21b740&scene=18#rd";
    public static String yyxz4 = "http://mp.weixin.qq.com/s?__biz=MzAwMjUyNDk1Nw==&mid=252104832&idx=1&sn=a8831af4b593ae5c1f3297c0bf21b740&scene=18#rd";

    //intent获取标识
    public static String baseItem = "baseItem";


}
