package com.afish.syl.lib.api.core.otto;

import java.util.ArrayList;
import java.util.List;

/**
 * 广播Push 字段消息数组
 * 作者：苏逸龙 on 2015-09-01 20:39
 * QQ：317616660
 */
public class PushExtraEvent {
    String msg;
    String NotificationTitle;
    String NotificationContent;
    List<PushFieldEvent> pushFieldEvents = new ArrayList<PushFieldEvent>();

    public PushExtraEvent() {
    }

    public PushExtraEvent(String notificationTitle, String notificationContent, List<PushFieldEvent> pushFieldEvents) {
        NotificationTitle = notificationTitle;
        NotificationContent = notificationContent;
        this.pushFieldEvents = pushFieldEvents;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getNotificationTitle() {
        return NotificationTitle;
    }

    public void setNotificationTitle(String notificationTitle) {
        NotificationTitle = notificationTitle;
    }

    public String getNotificationContent() {
        return NotificationContent;
    }

    public void setNotificationContent(String notificationContent) {
        NotificationContent = notificationContent;
    }

    public List<PushFieldEvent> getPushFieldEvents() {
        return pushFieldEvents;
    }

    public void setPushFieldEvents(List<PushFieldEvent> pushFieldEvents) {
        this.pushFieldEvents = pushFieldEvents;
    }
}
